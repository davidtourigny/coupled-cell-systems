######## R script for plotting example 2 #########

results <- as.matrix(read.table("results/vertices/u.txt"))
t = results[,1]
X1 = results[,2]
X2 = results[,3]
minlim = min(X1,X2)
maxlim = max(X1,X2)
pdf(file = "results/vertices/vertex_u.pdf")
par(mar=c(5, 4, 4, 6))
plot(t,X1,type="l",ylim=c(minlim,maxlim),col="blue",xlab="time",ylab="X")
lines(t,X2,col="red")
legend(max(t) - 15 ,maxlim,legend=c("X[0]","X[1]"),col=c("blue","red"),lty=1,bty="n")
dev.off()

results <- as.matrix(read.table("results/vertices/v.txt"))
t = results[,1]
X1 = results[,2]
X2 = results[,3]
minlim = min(X1,X2)
maxlim = max(X1,X2)
pdf(file = "results/vertices/vertex_v.pdf")
par(mar=c(5, 4, 4, 6))
plot(t,X1,type="l",ylim=c(minlim,maxlim),col="blue",xlab="time",ylab="X")
lines(t,X2,col="red")
legend(max(t) - 15 ,maxlim,legend=c("X[0]","X[1]"),col=c("blue","red"),lty=1,bty="n")
dev.off()

results <- as.matrix(read.table("results/example2.txt"))
t = results[,1]
X1 = results[,2]
X2 = results[,3]
X3 = results[,4]
X4 = results[,5]
minlim = min(X1,X2,X3,X4)
maxlim = max(X1,X2,X3,X4)
pdf(file = "results/example2.pdf")
par(mar=c(5, 4, 4, 6))
plot(t,X1,type="l",ylim=c(minlim,maxlim),col="blue",xlab="time",ylab="X")
lines(t,X2,col="red")
lines(t,X3,col="blue", lty="dashed")
lines(t,X4,col="red", lty="dashed")
legend(max(t) - 15 ,maxlim,legend=c("X[0]", "X[1]"), col=c("blue","red"), lty=1, bty="n")
dev.off()










