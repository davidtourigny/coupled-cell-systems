# Copyright (C) David S. Tourigny 2018, 2019 Columbia University Irving Medical Center,
#     New York, USA

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# import modules
from ccss.network import VertexValue, EdgeValue, PyGraph
from ccss.functions import Function, VertexFunction, EdgeFunction

# create and define vertex function with 2 dimensions and 2 parameters
vertex_function = VertexFunction("vertex_function", 2, 2)
x = vertex_function.get_vars()
p = vertex_function.get_params()
vertex_function.set_expression(1, p[0]*x[2] - p[1]*x[1])
vertex_function.set_expression(2, pow(x[1],2) - 1.0 - p[1]*x[2])

# create and define edge function with 2 x 2 dimensions and 2 parameters
edge_function = EdgeFunction("edge_function", 2, 2, 2)
x = edge_function.get_in_vars()
y = edge_function.get_out_vars()
p = edge_function.get_params()
edge_function.set_expression(1, p[0]*(y[1] - x[1]))
edge_function.set_expression(2, p[1]*(y[2] - x[2]))

# create vertex value and set function and parameters
vertex_value = VertexValue()
vertex_value.set_function(vertex_function)
vertex_value.set_params([1.0, 0.001])

# create edge value and set function and parameters
edge_value = EdgeValue()
edge_value.set_function(edge_function)
edge_value.set_params([0.0, 0.0])

# create PyGraph object with name "example2"
network = PyGraph("example2")

# add vertex "v" to network and set initial conditons then vertex value
network.add_vertex("v")
vertex_value.set_init_conds([0.0, 0.0])
network.set_vertex_value("v",vertex_value)

# add vertex "u" to network and set initial conditons then vertex value
network.add_vertex("u")
vertex_value.set_init_conds([0.9,0.0])
network.set_vertex_value("u",vertex_value)

# add edge ("v","u") and set edge value
network.add_edge("v","u")
network.set_edge_value("v","u",edge_value)

# add edge ("u","v") and set edge value
network.add_edge("u","v")
network.set_edge_value("u","v",edge_value)

# run simulation over interval [0.0, 50.0] with outputs every 0.1
network.simulate(0.0, 50.0, 0.1)
