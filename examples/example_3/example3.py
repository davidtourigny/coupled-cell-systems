# Copyright (C) David S. Tourigny 2018, 2019 Columbia University Irving Medical Center,
#     New York, USA

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# import modules
from ccss.network import VertexValue, EdgeValue, PyGraph
from ccss.functions import Function, VertexFunction, EdgeFunction

# create and define vertex function A with 2 dimensions and 2 parameters
function_A = VertexFunction("function_A", 2, 2)
x_A = function_A.get_vars()
p_A = function_A.get_params()
function_A.set_expression(1, p_A[0]*x_A[2] - p_A[1]*x_A[1])
function_A.set_expression(2, pow(x_A[1],2) - 1.0 - p_A[1]*x_A[2])

# create and define vertex function B with 1 dimension and 1 parameter
function_B = VertexFunction("function_B", 1, 1)
x_B = function_B.get_vars()
p_B = function_B.get_params()
function_B.set_expression(1, - p_B[0]*x_B[1])

# create and define edge function AB with 1 x 2 dimensions and 1 parameter
function_AB = EdgeFunction("function_AB", 1, 2, 1)
x_B = function_AB.get_in_vars()
y_A = function_AB.get_out_vars()
p_AB = function_AB.get_params()
function_AB.set_expression(1, - p_AB[0]*x_B[1]*(y_A[1] + y_A[2]))

# create and define edge function AB with 2 x 1 dimensions and 1 parameter
function_BA = EdgeFunction("function_BA", 2, 1, 1)
x_A = function_BA.get_in_vars()
y_B = function_BA.get_out_vars()
p_BA = function_BA.get_params()
function_BA.set_expression(1, - p_BA[0]*x_A[1]*y_B[1])
function_BA.set_expression(2, - p_BA[0]*x_A[2]*y_B[1])

# create vertex value A and set function A, initial conditions and parameters
value_A = VertexValue()
value_A.set_function(function_A)
value_A.set_init_conds([0.0, 0.0])
value_A.set_params([1.0, 0.001])

# create vertex value B and set function B, initial conditions and parameters
value_B = VertexValue()
value_B.set_function(function_B)
value_B.set_init_conds([10.0])
value_B.set_params([0.1])

# create edge value AB and set function AB and parameters
value_AB = EdgeValue()
value_AB.set_function(function_AB)
value_AB.set_params([0.01])

# create edge value BA and set function BA and parameters
value_BA = EdgeValue()
value_BA.set_function(function_BA)
value_BA.set_params([0.01])

# create PyGraph object with name "example3"
network = PyGraph("example3")

# add vertex "A" to network and set value A
network.add_vertex("A")
network.set_vertex_value("A", value_A)

# add vertex "B" to network and set value B
network.add_vertex("B")
network.set_vertex_value("B", value_B)

# add edge "AB" and set value AB
network.add_edge("A","B")
network.set_edge_value("A", "B", value_AB)

# add edge "BA" and set value BA
network.add_edge("B","A")
network.set_edge_value("B", "A", value_BA)

# run simulation over interval [0.0, 50.0] with outputs every 0.1
network.simulate(0.0, 50.0, 0.1)
