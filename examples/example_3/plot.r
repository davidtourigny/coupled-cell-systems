######## R script for plotting example 3 #########

results <- as.matrix(read.table("results/vertices/A.txt"))
t = results[,1]
X1 = results[,2]
X2 = results[,3]
minlim = min(X1,X2)
maxlim = max(X1,X2)
pdf(file = "results/vertices/vertex_A.pdf")
par(mar=c(5, 4, 4, 6))
plot(t,X1,type="l",ylim=c(minlim,maxlim),col="blue",xlab="time",ylab="X")
lines(t,X2,col="red")
legend(max(t) - 15 ,maxlim,legend=c("X[0]","X[1]"),col=c("blue","red"),lty=1,bty="n")
dev.off()

results <- as.matrix(read.table("results/vertices/B.txt"))
t = results[,1]
X = results[,2]
minlim = min(X)
maxlim = max(X)
pdf(file = "results/vertices/vertex_B.pdf")
par(mar=c(5, 4, 4, 6))
plot(t,X,type="l",ylim=c(minlim,maxlim),col="blue",xlab="time",ylab="X")
legend(max(t) - 15 ,maxlim,legend=c("X"),col=c("blue"),lty=1,bty="n")
dev.off()










