######## R script for plotting example 1 #########

results <- as.matrix(read.table("results/example1.txt"))
t = results[,1]
X1 = results[,2]
X2 = results[,3]
minlim = min(X1,X2)
maxlim = max(X1,X2)
pdf(file = "results/example1.pdf")
par(mar=c(5, 4, 4, 6))
plot(t,X1,type="l",ylim=c(minlim,maxlim),col="blue",xlab="time",ylab="X")
lines(t,X2,col="red")
legend(max(t) - 15 ,maxlim,legend=c("Vertex u","Vertex v"),col=c("blue","red"),lty=1,bty="n")
dev.off()












