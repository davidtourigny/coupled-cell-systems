# Copyright (C) David S. Tourigny 2018, 2019 Columbia University Irving Medical Center,
#     New York, USA

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# import modules
from ccss.network import VertexValue, PyGraph
from ccss.functions import Function, VertexFunction

# create and define function with 1 dimension and 1 parameter
function = VertexFunction("function", 1, 1)
x = function.get_vars()
p = function.get_params()
function.set_expression(1, - p[0]*x[1])

# create vertex value and set function
value = VertexValue()
value.set_function(function)

# create PyGraph object with name "example1"
network = PyGraph("example1")

# add vertex "v" to network and set initial conditions, function parameters, and value
network.add_vertex("v")
value.set_init_conds([10.0])
value.set_params([0.1])
network.set_vertex_value("v", value)

# add vertex "u" to network and set alternative initial conditons, function parameters, and value
network.add_vertex("u")
value.set_init_conds([15.0])
value.set_params([0.2])
network.set_vertex_value("u", value)

# run simulation over interval [0.0, 50.0] with outputs every 0.1
network.simulate(0.0, 50.0, 0.1)
