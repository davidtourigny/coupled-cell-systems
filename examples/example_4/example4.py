# Copyright (C) David S. Tourigny 2018, 2019 Columbia University Irving Medical Center,
#     New York, USA

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# import modules
from ccss.network import VertexValue, EdgeValue, PyGraph
from ccss.functions import Function, VertexFunction, EdgeFunction
import numpy as np

# create and define vertex function "FitzHughNagumo" with 2 dimensions and 3 parameters
vertex_function = VertexFunction("FitzHughNagumo", 2, 2)
x = vertex_function.get_vars()
p = vertex_function.get_params()
vertex_function.set_expression(1, (x[1] - pow(x[1],3)/3 - x[2])/p[0])
vertex_function.set_expression(2, x[1] + p[1])

# create and define edge function "Coupling" with 2 x 2 dimensions and 1 parameter
edge_function = EdgeFunction("Coupling", 2, 2, 1)
x = edge_function.get_in_vars()
y = edge_function.get_out_vars()
p = edge_function.get_params()
edge_function.set_expression(1, p[0]*(y[1] - x[1]))
edge_function.set_expression(2, 0.0)

# create vertex value and set function and initial conditions
vertex_value = VertexValue()
vertex_value.set_function(vertex_function)
vertex_value.set_init_conds([0.0, 0.0])

# create edge value and set function
edge_value = EdgeValue()
edge_value.set_function(edge_function)

# create PyGraph object with name "example4"
network = PyGraph("example4")

# parameters for model
epsilon = 0.01
N = 10
C = 0.05
p = 0.75

# add N vertices to network with randomized parameter a
for i in range(N) :
    network.add_vertex("vertex_" + str(i))
    a = 2*np.random.rand()
    vertex_value.set_params([epsilon, a])
    network.set_vertex_value("vertex_" + str(i), vertex_value)

# add edges symmetrically to graph with probability 1 - p, weight in (0,1], and total coupling strength C
for i in range(N) :
    for j in range(i) :
        w = np.random.rand()
        if (w > p) :
            edge_value.set_params([(w - p)*C/(1 - p)])
            network.add_edge("vertex_" + str(i), "vertex_" + str(j))
            network.set_edge_value("vertex_" + str(i), "vertex_" + str(j), edge_value)
            network.add_edge("vertex_" + str(j), "vertex_" + str(i))
            network.set_edge_value("vertex_" + str(j), "vertex_" + str(i), edge_value)

# run simulation over interval [0.0, 50.0] with outputs every 0.1
network.simulate(0.0, 50.0, 0.1)
