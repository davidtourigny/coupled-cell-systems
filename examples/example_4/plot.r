######## R script for plotting example 4 #########

files <- list.files(path="results/vertices", pattern="*.txt", full.names=TRUE, recursive=FALSE)

vertex_plot <- function(x){
    results <- as.matrix(read.table(x))
    t = results[,1]
    X1 = results[,2]
    X2 = results[,3]
    minlim = min(X1,X2)
    maxlim = max(X1,X2)
    filename = paste(sub("*.txt", "",x),".pdf", sep = "")
    pdf(file = filename)
    par(mar=c(5, 4, 4, 6))
    plot(t,X1,type="l",ylim=c(minlim,maxlim),col="blue",xlab="time",ylab="X")
    lines(t,X2,col="red")
    legend(max(t) - 15 ,maxlim,legend=c("u","v"),col=c("blue","red"),lty=1,bty="n")
    dev.off()
}

lapply(files, vertex_plot)









