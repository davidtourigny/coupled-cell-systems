## *CCSS* examples

This directory contains the following content:

* [`./example_1/`](./example_1/): directory containing files for *CCSS* example 1
* [`./example_2/`](./example_2/): directory containing files for *CCSS* example 2
* [`./example_3/`](./example_3/): directory containing files for *CCSS* example 3
* [`./example_4/`](./example_4/): directory containing files for *CCSS* example 4
* [`./README.md`](./README.md): this document

Details for each example are provided in the documentation [`ccss_examples.pdf`](../docs/examples/ccss_examples.pdf).

