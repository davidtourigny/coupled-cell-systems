# Copyright (C) David S. Tourigny 2018, 2019 Columbia University Irving Medical Center,
#     New York, USA

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import os, sysconfig
import sympy
from . import jit

# non-member method for writing cpp file
def WriteIndent(theFile, indentCount, text) :
    while (indentCount > 0):
        text = "    " + text
        indentCount -= 1
    theFile.write(text)

cppfilePath = os.path.join("functionlib.cpp")

### Definition of Function base class
#####################################
class Function() :
    
    # dict of class instances
    Instances = {}
    
    # init
    def __init__(self, name, header) :
        self._id = name
        self._header = header
        self.AddInstance(self)

    # str
    def __str__(self) :
        return "%s" % (self.id)
    
    # repr
    def __repr__(self) :
        return  "<%s %s at 0x%x>" % (self.__class__.__name__, self.id, id(self))

    # writes function to cpp file
    def write_function(self) :
        with open(cppfilePath, "a") as cppFile :
            t = 0
            WriteIndent(cppFile, t, "extern \"C\" void " + str(self) + self.header)
            WriteIndent(cppFile, t, "{\n")
            for i in range(self.dimension) :
                WriteIndent(cppFile, t + 1, "ypval[pos + " + str(i) + "] += " + sympy.ccode(self.expression[i]) + "; \n")
            WriteIndent(cppFile, t, "}\n\n")

    # properties
    @property
    def id(self) :
        return self._id

    @property
    def header(self) :
        return self._header
    
    # class methods
    @classmethod
    def GetInstances(self) :
        return self.Instances.copy()
    
    @classmethod
    def AddInstance(self, instance) :
        self.Instances[str(instance)] = instance

    @classmethod
    def RemoveInstance(self, instance_id) :
        del self.Instances[instance_id]

    @classmethod
    def Compile(self) :
        with open(cppfilePath, "w") as cppFile :
            cppFile.write("#include <iostream> \n#include <vector> \n#include <cmath> \n#include <sundials/sundials_types.h> \n\n")
        for key in self.Instances :
            self.Instances[key].write_function()
        jit.compile()

### Definition of VertexFunction derived class
##############################################
class VertexFunction(Function) :
    
    # init
    def __init__(self, name, dim, num_params) :
        Function.__init__(self, name, "(realtype t, realtype *yval, realtype *ypval, std::vector<double> params, int pos)\n")
        self._dimension = dim
        self._num_parameters = num_params
        self._expression = dim*[0.0]
    
    # generate variable list for vertex function expression
    def get_vars(self) :
        t = sympy.Symbol("t")
        X = [t]
        for i in range(self.dimension) :
            x = sympy.Symbol("yval[pos + " + str(i) + "]")
            X.append(x)
        return X

    # generate parameter list for vertex function expression
    def get_params(self) :
        params = []
        for i in range(self.num_parameters) :
            p = sympy.Symbol("params[" + str(i) + "]")
            params.append(p)
        return params
    
    # set expression for ith variable
    def set_expression(self, i, expr) :
        self.expression[i - 1] = expr
    
    # properties
    @property
    def dimension(self) :
        return self._dimension

    @property
    def num_parameters(self) :
        return self._num_parameters

    @property
    def expression(self) :
        return self._expression

### Definition of EdgeFunction derived class
############################################

class EdgeFunction(Function) :

    # init
    def __init__(self, name, x_dim, y_dim, num_params) :
        Function.__init__(self, name, "(realtype t, realtype *yval, realtype *ypval, std::vector<double> params, int pos, int nbr_pos)\n")
        self._dimension = x_dim
        self._out_dimension = y_dim
        self._num_parameters = num_params
        self._expression = x_dim*[0.0]
        
    # generate variable list for vertex function expression
    def get_in_vars(self) :
        t = sympy.Symbol("t")
        X = [t]
        for i in range(self.dimension) :
            x = sympy.Symbol("yval[pos + " + str(i) + "]")
            X.append(x)
        return X

    # generate variable list for vertex function expression
    def get_out_vars(self) :
        t = sympy.Symbol("t")
        Y = [t]
        for i in range(self.out_dimension) :
            y = sympy.Symbol("yval[nbr_pos + " + str(i) + "]")
            Y.append(y)
        return Y

    # generate parameter list for vertex function expression
    def get_params(self) :
        params = []
        for i in range(self.num_parameters) :
            p = sympy.Symbol("params[" + str(i) + "]")
            params.append(p)
        return params
    
    # set expression for ith in variable
    def set_expression(self, i, expr) :
        self.expression[i - 1] = expr

    # properties
    @property
    def dimension(self) :
        return self._dimension

    @property
    def out_dimension(self) :
        return self._out_dimension
    
    @property
    def num_parameters(self) :
        return self._num_parameters

    @property
    def expression(self) :
        return self._expression
