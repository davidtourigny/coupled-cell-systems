// Copyright (C) David S. Tourigny 2018, 2019 Columbia University Irving Medical Center,
//     New York, USA

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <dlfcn.h>
#include <cvode/cvode.h>               //prototypes for CVODE fcts, consts
#include <nvector/nvector_serial.h>    //access to serial N_Vector
#include <sunmatrix/sunmatrix_dense.h> //access to dense SUNMatrix
#include <sunlinsol/sunlinsol_dense.h> //access to dense SUNLinearSolver
#include <cvode/cvode_direct.h>        //access to CVDls interface
#include <sundials/sundials_types.h>   //defs of realtype, sunindextype
#include "network.h"

/*
 *---------------------
 * Macros and typedefs.
 *---------------------
 */

///Macros for use in non-member functions
/////////////////////////////////////////
#define NEQ   model.get_neq()
#define TSTOP model.get_tstop()
#define TOUT  model.get_tout()

///Macro for N vector elements
//////////////////////////////
#define Ith(v,i)    NV_Ith_S(v,i)

///Typedefs for user-supplied functions
///////////////////////////////////////
typedef void (*VertexFunction)(realtype, realtype *, realtype *, std::vector<double>, int);
typedef void (*EdgeFunction)(realtype, realtype *, realtype *, std::vector<double>, int, int);

/*
 *--------------
 * Declarations.
 *--------------
 */

///Graph and Vertex declarations
////////////////////////////////
struct Vertex{
    std::string name;
    int dimension;
    int position;
    std::vector<double> parameters;
    VertexFunction vertex_function;
    void eval_function(realtype t, realtype *yval, realtype *ypval){ vertex_function(t, yval, ypval, parameters, position); }
    std::vector<EdgeFunction> edge_functions;
    std::vector< std::vector<double> > edge_parameters;
    void eval_edge_function(int nbr, realtype t, realtype *yval, realtype *ypval, int nbr_position){ edge_functions[nbr](t, yval, ypval, edge_parameters[nbr], nbr_position, position); }
    int num_neighbors;
    std::vector<int> neighbor_list;
};

struct Graph{
    std::vector<Vertex> vertex_list;
};

///CCNS_MODEL class declaration
///////////////////////////////
class CCSS_MODEL
{
private:
    int neq;
    realtype tstop, tout;
    Graph network;
    realtype current_t;
    N_Vector current_y;
    
public:
    CCSS_MODEL(Graph &, int, std::vector<double>, double, double, double);
    void set_network_data();
    int get_neq();
    realtype get_tstop(), get_tout();
    void starting_values(realtype &, realtype *), rhs(realtype, realtype *, realtype *);
    void update_kinetics(realtype, N_Vector);
    realtype current_tval();
    N_Vector current_yval();
    void clear();
};

///Non-member function declarations
///////////////////////////////////

///SUNDIALS CVODE
static int f(realtype t, N_Vector y, N_Vector ydot, void *user_data);
static void PrintOutput(realtype, N_Vector, CCSS_MODEL &, std::ofstream &), PrintFinalStats(void *cvode_mem);
static int check_flag(void *flagvalue, const char *funcname, int opt);

///Simulation
int simulate(CCSS_MODEL &, std::ofstream &, double, double, bool);

/*
 *-----------------------------
 * CCSS_MODEL member functions.
 *-----------------------------
 */

CCSS_MODEL::CCSS_MODEL(Graph &input_network, int num_equations, std::vector<double> initial_conditions, double t_start, double t_stop, double t_out)
{
    ///Here we set network and equation data
    network = input_network;
    neq = num_equations;
    
    ///Here we set initial conditions for CVode variables
    current_y = N_VNew_Serial(neq);
    for(unsigned int i=0; i<neq; i++){
        Ith(current_y,i) = initial_conditions[i];
    }
    
    ///Here we set integration times
    tstop = RCONST(t_stop);
    tout = RCONST(t_out);
    current_t = RCONST(t_start);
}

int CCSS_MODEL::get_neq()
{
    return neq;
}

realtype CCSS_MODEL::get_tstop()
{
    return tstop;
}

realtype CCSS_MODEL::get_tout()
{
    return tout;
}

/*
 * Set starting values for integration
 */

void CCSS_MODEL::starting_values(realtype &tval, realtype *yval)
{
    tval = current_t;
    for(unsigned int i=0; i<neq; i++){
        yval[i] = Ith(current_y,i);
    }
}

/*
 * Update current values of kinetic parameters
 */

void CCSS_MODEL::update_kinetics(realtype tval, N_Vector yval)
{
    current_t = tval;
    for(unsigned int i=0; i<neq; i++){
        Ith(current_y,i) = Ith(yval,i);
    }
}

/*
 * Calculate right hand side of ODE, to be specified by model
 */

void CCSS_MODEL::rhs(realtype t, realtype *yval, realtype *ypval)
{
    ///Here we blank ypvals
    for(unsigned int i=0; i<neq; i++){
        ypval[i] = RCONST(0.0);
    }
    
    ///Here we set ypvals
    for(unsigned int v=0; v<network.vertex_list.size(); v++){ //loop over vertices
        Vertex vertex = network.vertex_list[v]; //copy of vertex
        vertex.eval_function(t,yval,ypval); //evaluate vertex function
        for(unsigned int u=0; u<vertex.neighbor_list.size(); u++){ //loop over neighbors
            int nbr_position = network.vertex_list[vertex.neighbor_list[u]].position; //neighbor position
            vertex.eval_edge_function(u,t,yval,ypval,nbr_position); //evaluate edge function
        }
    }
}

/*
 * Return current kinetic values
 */

realtype CCSS_MODEL::current_tval()
{
    return current_t;
}

N_Vector CCSS_MODEL::current_yval()
{
    return current_y;
}

/*
 * Clear model N_Vectors
 */

void CCSS_MODEL::clear()
{
    N_VDestroy(current_y);
}

/*
 *----------------------
 * Non-member functions.
 *----------------------
 */

/*
 * Compute function f(t,y).
 */

static int f(realtype t, N_Vector y, N_Vector ydot, void *user_data)
{
    ((CCSS_MODEL*)user_data)->rhs(t,N_VGetArrayPointer(y),N_VGetArrayPointer(ydot));
    return 0;
}

/*
 * Print kinetic values to results file
 */

static void PrintOutput(realtype t, N_Vector y, CCSS_MODEL &model, std::ofstream &outputfile)
{
    outputfile << t;
    for(unsigned int i=0; i<NEQ; i++){
        if(i<NEQ-1){
            outputfile << " " << Ith(y,i);
        }
        else{
            outputfile << " " << Ith(y,i) << std::endl;
        }
    }
}

/*
 * Print final integrator statistics
 */

static void PrintFinalStats(void *cvode_mem)
{
    long int nst, nfe, nsetups, nje, nfeLS, nni, ncfn, netf, nge;
    int flag;
    
    flag = CVodeGetNumSteps(cvode_mem, &nst);
    check_flag(&flag, "CVodeGetNumSteps", 1);
    flag = CVodeGetNumRhsEvals(cvode_mem, &nfe);
    check_flag(&flag, "CVodeGetNumRhsEvals", 1);
    flag = CVodeGetNumLinSolvSetups(cvode_mem, &nsetups);
    check_flag(&flag, "CVodeGetNumLinSolvSetups", 1);
    flag = CVodeGetNumErrTestFails(cvode_mem, &netf);
    check_flag(&flag, "CVodeGetNumErrTestFails", 1);
    flag = CVodeGetNumNonlinSolvIters(cvode_mem, &nni);
    check_flag(&flag, "CVodeGetNumNonlinSolvIters", 1);
    flag = CVodeGetNumNonlinSolvConvFails(cvode_mem, &ncfn);
    check_flag(&flag, "CVodeGetNumNonlinSolvConvFails", 1);
    flag = CVDlsGetNumJacEvals(cvode_mem, &nje);
    check_flag(&flag, "CVDlsGetNumJacEvals", 1);
    flag = CVDlsGetNumRhsEvals(cvode_mem, &nfeLS);
    check_flag(&flag, "CVDlsGetNumRhsEvals", 1);
    flag = CVodeGetNumGEvals(cvode_mem, &nge);
    check_flag(&flag, "CVodeGetNumGEvals", 1);
    
    printf("\nFinal Statistics:\n");
    printf("Steps = %-6ld RhsEvals  = %-6ld LinSolvSetups = %-6ld lsRhsEvals = %-6ld JacEvals = %ld\n",
           nst, nfe, nsetups, nfeLS, nje);
    printf("NonlinSolvIters = %-6ld NonlinSolvConvFails = %-6ld ErrTestFails = %-6ld GEvals = %ld\n \n",
           nni, ncfn, netf, nge);
}

/*
 * Check function return value...
 *   opt == 0 means SUNDIALS function allocates memory so check if
 *            returned NULL pointer
 *   opt == 1 means SUNDIALS function returns a flag so check if
 *            flag >= 0
 *   opt == 2 means function allocates memory so check if returned
 *            NULL pointer
 */

static int check_flag(void *flagvalue, const char *funcname, int opt)
{
    int *errflag;
    
    /* Check if SUNDIALS function returned NULL pointer - no memory allocated */
    if (opt == 0 && flagvalue == NULL) {
        fprintf(stderr, "\nSUNDIALS_ERROR: %s() failed - returned NULL pointer\n\n",
                funcname);
        return(1); }
    
    /* Check if flag < 0 */
    else if (opt == 1) {
        errflag = (int *) flagvalue;
        if (*errflag < 0) {
            fprintf(stderr, "\nSUNDIALS_ERROR: %s() failed with flag = %d\n\n",
                    funcname, *errflag);
            return(1); }}
    else if (opt == 2 && flagvalue == NULL) {
        fprintf(stderr, "\nMEMORY_ERROR: %s() failed - returned NULL pointer\n\n",
                funcname);
        return(1); }
    
    return(0);
}

/*
 * Simulation of model
 */

int simulate(CCSS_MODEL &model, std::ofstream &outputfile, double rel_tolerance, double abs_tolerance, bool solver)
{
    ///Here we declare and initialize required objects for CVode
    realtype reltol, t0, t, tout;
    realtype *yval;
    N_Vector y, abstol;
    SUNMatrix A;
    SUNLinearSolver LS;
    void *cvode_mem;
    int flag, flagr;
    y = abstol = NULL;
    A = NULL;
    LS = NULL;
    cvode_mem = NULL;
    
    ///Here we allocate N-vectors
    y = N_VNew_Serial(NEQ);
    if (check_flag((void *)y, "N_VNew_Serial", 0)) return(1);
    abstol = N_VNew_Serial(NEQ);
    if (check_flag((void *)abstol, "N_VNew_Serial", 0)) return(1);
    
    /// Here we set tolerances
    reltol = RCONST(rel_tolerance);
    for(unsigned int i=0; i<NEQ; i++){
        Ith(abstol,i) = RCONST(abs_tolerance);
    }
    
    /// Here we create and initialize  y, y', and set integration limits, using model
    yval  = N_VGetArrayPointer(y);
    model.starting_values(t0,yval);
    
    ///Here we set up the CVODE memory object and check flags at each step
    if(solver){
        cvode_mem = CVodeCreate(CV_BDF); //create CVode object with BDF
        std::cout << "Initialized CVode with solver CV_BDF" << std::endl;
    }
    else{
        cvode_mem = CVodeCreate(CV_ADAMS); //create CVode object with ADAMS
        std::cout << "Initialized CVode with solver CV_ADAMS" << std::endl;
    }
    std::cout << "rel tolerance = " << rel_tolerance << ", abs tolerance = " << abs_tolerance << std::endl;
    if (check_flag((void *)cvode_mem, "CVodeCreate", 0)) return(1);
    CCSS_MODEL *user_data = &model; //create user data (reference to model)
    flag = CVodeSetUserData(cvode_mem, user_data); //set reference to model as user data
    if (check_flag(&flag, "CVodeSetUserData", 1)) return(1);
    flag = CVodeInit(cvode_mem, f, t0, y); //initialize CVode memory
    if (check_flag(&flag, "CVodeInit", 1)) return(1);
    flag = CVodeSVtolerances(cvode_mem, reltol, abstol); //set tolerances
    if (check_flag(&flag, "CVodeSVtolerances", 1)) return(1);
    A = SUNDenseMatrix(NEQ, NEQ); //create dense SUNMatrix for use in linear solves
    if(check_flag((void *)A, "SUNDenseMatrix", 0)) return(1);
    LS = SUNDenseLinearSolver(y, A); //create dense SUNLinearSolver object
    if(check_flag((void *)LS, "SUNDenseLinearSolver", 0)) return(1);
    flag = CVDlsSetLinearSolver(cvode_mem, LS, A); //attach the matrix and linear solver
    if(check_flag(&flag, "CVDlsSetLinearSolver", 1)) return(1);
    flag = CVDlsSetJacFn(cvode_mem, NULL); //set the default Jacobian routine
    if(check_flag(&flag, "CVDlsSetJacFn", 1)) return(1);
    flag = CVodeSetStopTime(cvode_mem, TSTOP); //set the default Jacobian routine
    if(check_flag(&flag, "CVodeSetStopTime", 1)) return(1);
    
    
    ///Here we integrate, printing results at intervals of length TOUT and break out at TSTOP
    tout = TOUT;
    PrintOutput(t,y,model,outputfile);
    while(1) {
        flag = CVode(cvode_mem, tout, y, &t, CV_NORMAL);
        if(check_flag(&flag, "CVode", 1)) break;
        if(flag == CV_TSTOP_RETURN){
            std::cout << "CVode reached tstop successfully" << std::endl;
            PrintOutput(t,y,model,outputfile);
            break;
        }
        if(flag == CV_SUCCESS){
            model.update_kinetics(t,y);
            PrintOutput(t,y,model,outputfile);
            tout += TOUT;
        }
    }
    
    ///Here we print final stats and free memory
    PrintFinalStats(cvode_mem);
    N_VDestroy(y);
    N_VDestroy(abstol);
    CVodeFree(&cvode_mem);
    SUNLinSolFree(LS);
    SUNMatDestroy(A);
    return flag;
}

