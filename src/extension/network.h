// Copyright (C) David S. Tourigny 2018, 2019 Columbia University Irving Medical Center,
//     New York, USA

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <boost/python.hpp>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <vector>
#include <fstream>
#include <stdio.h>

namespace bp = boost::python;

/*
 *---------------
 * Value Classes.
 *---------------
 */

///Definition of VertexValue
////////////////////////////
class VertexValue
{
private:
    int dimension = 0;
    int num_parameters = 0;
    std::vector<double> initial_conditions;
    std::vector<double> parameters;
    bp::object user_data;
    
public:
    ///Wraped through python
    std::string function_id;
    
    void set_function(bp::object function_object){
        function_id = bp::extract<std::string>(function_object.attr("id"));
        dimension = bp::extract<int>(function_object.attr("dimension"));
        num_parameters = bp::extract<int>(function_object.attr("num_parameters"));
    }
    
    void set_init_conds(bp::list init_conds){
        if(len(init_conds) == dimension){
            std::vector<double> tmp_init_conds(dimension,0.0);
            for(unsigned int i=0; i<dimension; i++){
                tmp_init_conds[i] = bp::extract<double>(init_conds[i]);
            }
            initial_conditions = tmp_init_conds;
        }
        else{
            std::cout << "Wrong number of initial conditions! Expected list of length " << dimension << ", received list of length " << len(init_conds) << "." << std::endl;
            std::cout << "Initial conditions not set. Check length or try setting function first." << std::endl;
        }
    }
    
    void set_params(bp::list parms){
        if(len(parms) == num_parameters){
            std::vector<double> tmp_parms(num_parameters,0.0);
            for(unsigned int i=0; i<num_parameters; i++){
                tmp_parms[i] = bp::extract<double>(parms[i]);
            }
            parameters = tmp_parms;
        }
        else{
            std::cout << "Wrong number of parmeters! Expected list of length " << num_parameters << ", received list of length " << len(parms) << "." << std::endl;
            std::cout << "Parameters not set. Check length or try setting function first." << std::endl;
        }
    }
    
    bp::object get_user_data(){
        return user_data;
    }
    
    void set_user_data(bp::object usr_data){
        user_data = usr_data;
    }
    
    ///Acessed through C++ only
    int get_dimension(){
        return dimension;
    }
    
    std::vector<double> get_init_conds(){
        return initial_conditions;
    }
    
    std::vector<double> get_parameters(){
        return parameters;
    }
};

///Definition of EdgeValue
//////////////////////////
class EdgeValue
{
private:
    int num_parameters = 0;
    std::vector<double> parameters;
    bp::object user_data;
    
public:
    ///Wraped through python
    std::string function_id;
    
    void set_function(bp::object function_object){
        function_id = bp::extract<std::string>(function_object.attr("id"));
        num_parameters = bp::extract<int>(function_object.attr("num_parameters"));
    }
    
    void set_params(bp::list parms){
        if(len(parms) == num_parameters){
            std::vector<double> tmp_parms(num_parameters,0.0);
            for(unsigned int i=0; i<num_parameters; i++){
                tmp_parms[i] = bp::extract<double>(parms[i]);
            }
            parameters = tmp_parms;
        }
        else{
            std::cout << "Wrong number of parmeters! Expected list of length " << num_parameters << ", received list of length " << len(parms) << "." << std::endl;
            std::cout << "Parameters not set. Check length or try setting function first." << std::endl;
        }
    }
    
    bp::object get_user_data(){
        return user_data;
    }
    
    void set_user_data(bp::object usr_data){
        user_data = usr_data;
    }
    
    ///Acessed through C++ only
    std::vector<double> get_parameters(){
        return parameters;
    }
};

/*
 *-------------------
 * Graph ADT Classes.
 *-------------------
 */

///Definition of PyVertex
/////////////////////////
struct PyVertex
{
    PyVertex(std::string key){
        name = key;
    }
    
    std::string get_id(){
        return name;
    }
    
    void add_neighbor(std::string nbr){
        EdgeValue outvalue;
        connected_to[nbr] = outvalue;
    }
    
    void remove_neighbor(std::string nbr){
        if(connected_to.has_key(nbr)){
            connected_to[nbr].del();
        }
        else{
            std::cout << nbr << " is not an outneighbor of PyVertex " << name << "! No outneighbor has been removed." << std::endl;
        }
    }
    
    bp::list neighbors(){
        return connected_to.keys();
    }
    
    VertexValue get_value(){
        return value;
    }
    
    void set_value(VertexValue vertex){
        value = vertex;
    }
    
    bp::object get_outvalue(std::string nbr){
        if(connected_to.has_key(nbr)){
            return connected_to[nbr];
        }
        else{
            std::cout << nbr << " is not an outneighbor of PyVertex " << name << "! An empty outvalue has been returned." << std::endl;
            bp::object outvalue;
            return outvalue;
        }
    }
    
    void set_outvalue(std::string nbr, EdgeValue outvalue){
        if(connected_to.has_key(nbr)){
            connected_to[nbr] = outvalue;
        }
        else{
            std::cout << nbr << " is not an outneighbor of PyVertex " << name << "! No outvalue has been set." << std::endl;
        }
    }
    
    std::string name;
    VertexValue value;
    bp::dict connected_to;
};

///Definition of PyGraph
////////////////////////
struct PyGraph
{
    PyGraph(std::string text){
        num_vertices = 0;
        name = text;
    }
    
    bool adjacent(std::string f, std::string t){
        if(vertex_list.has_key(f) && vertex_list.has_key(t)){
            bp::object vf = vertex_list[f];
            PyVertex vertex_f = bp::extract<PyVertex>(vf);
            return vertex_f.connected_to.has_key(t);
        }
        else{
            return 0;
        }
    }
    
    bp::list neighbors(std::string n){
        if(vertex_list.has_key(n)){
            bp::object vn = vertex_list[n];
            PyVertex vertex_n = bp::extract<PyVertex>(vn);
            return vertex_n.neighbors();
        }
        else{
            bp::list list;
            std::cout << "PyGraph object does not contain vertex " << n << "! A empty list has been returned." << std::endl;
            return list;
        }
    }
    
    PyVertex add_vertex(std::string key){
        PyVertex newvertex(key);
        vertex_list[key] = newvertex;
        num_vertices++;
        return newvertex;
    }
    
    void remove_vertex(std::string n){
        if(vertex_list.has_key(n)){
            vertex_list[n].del();
            num_vertices--;
        }
        else{
            std::cout << "PyGraph object does not contain vertex " << n << "! No vertex has been removed." << std::endl;
        }
    }
    
    void add_edge(std::string f, std::string t){
        if(!vertex_list.has_key(f)){
            PyVertex nv = add_vertex(f);
        }
        if(!vertex_list.has_key(t)){
            PyVertex nv = add_vertex(t);
        }
        bp::object vf = vertex_list[f], vt = vertex_list[t];
        PyVertex vertex_f = bp::extract<PyVertex>(vf);
        std::string vertex_t = bp::extract<std::string>(vt.attr("get_id")());
        vertex_f.add_neighbor(vertex_t);
    }
    
    void remove_edge(std::string f, std::string t){
        if(vertex_list.has_key(f)){
            bp::object vf = vertex_list[f];
            PyVertex &vertex_f = bp::extract<PyVertex&>(vf);
            vertex_f.remove_neighbor(t);
        }
        else{
            std::cout << "PyGraph object does not contain vertex " << f << "! No edge has been removed." << std::endl;
        }
    }
    
    VertexValue get_vertex_value(std::string n){
        if(vertex_list.has_key(n)){
            bp::object vn = vertex_list[n];
            return bp::extract<VertexValue>(vn.attr("get_value")());
        }
        else{
            VertexValue value;
            std::cout << "PyGraph object does not contain vertex " << n << "! A default VertexValue instance has been returned." << std::endl;
            return value;
        }
    }
    
    void set_vertex_value(std::string n, VertexValue value){
        if(vertex_list.has_key(n)){
            bp::object vn = vertex_list[n];
            PyVertex &vertex_n = bp::extract<PyVertex&>(vn);
            vertex_n.set_value(value);
        }
        else{
            std::cout << "PyGraph object does not contain vertex " << n << "! No VertexValue has been set." << std::endl;
        }
    }
    
    EdgeValue get_edge_value(std::string f, std::string t){
        if(vertex_list.has_key(f)){
            bp::object vf = vertex_list[f];
            return bp::extract<EdgeValue>(vf.attr("get_outvalue")(t));
        }
        else{
            EdgeValue value;
            std::cout << "PyGraph object does not contain vertex " << f << "! A default EdgeValue instance has been returned." << std::endl;
            return value;
        }
    }
    
    void set_edge_value(std::string f, std::string t, EdgeValue value){
        if(vertex_list.has_key(f)){
            bp::object vf = vertex_list[f];
            PyVertex &vertex_f = bp::extract<PyVertex&>(vf);
            vertex_f.set_outvalue(t,value);
        }
        else{
            std::cout << "PyGraph object does not contain vertex " << f << "! No EdgeValue has been set." << std::endl;
        }
    }
    
    void simulate(double, double, double, bool solver = 0);
    
    double get_rel_tolerance(){
        return rel_tolerance;
    }
    
    double get_abs_tolerance(){
        return abs_tolerance;
    }
    
    void set_rel_tolerance(double rel_tol){
        rel_tolerance = rel_tol;
    }
    
    void set_abs_tolerance(double abs_tol){
        abs_tolerance = abs_tol;
    }
    
    std::string name;
    bp::dict vertex_list;
    int num_vertices;
    double rel_tolerance = 10e-4, abs_tolerance = 10e-4;
};
