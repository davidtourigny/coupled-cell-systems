// Copyright (C) David S. Tourigny 2018, 2019 Columbia University Irving Medical Center,
//     New York, USA

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "model.h"
#include <time.h>
#include <sys/stat.h>
#include <istream>

/*
 *----------------------
 * network wrapper.
 *----------------------
 */

/*
 * Wrap network module
 */

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(simulate_overloads,PyGraph::simulate,3,4)

BOOST_PYTHON_MODULE(network)
{
    ///Wrap VertexValue
    ///////////////////
    bp::class_<VertexValue>("VertexValue")
    .def("set_function", &VertexValue::set_function)
    .def("set_init_conds", &VertexValue::set_init_conds)
    .def("set_params", &VertexValue::set_params)
    .def_readonly("function_id", &VertexValue::function_id)
    .add_property("user_data", &VertexValue::get_user_data, &VertexValue::set_user_data)
    ;
    
    ///Wrap EdgeValue
    /////////////////
    bp::class_<EdgeValue>("EdgeValue")
    .def("set_function", &EdgeValue::set_function)
    .def("set_params", &EdgeValue::set_params)
    .def_readonly("function_id", &EdgeValue::function_id)
    .add_property("user_data", &EdgeValue::get_user_data, &EdgeValue::set_user_data)
    ;
    
    ///Wrap PyVertex
    ////////////////
    bp::class_<PyVertex>("PyVertex", bp::init<std::string>())
    .def("get_id", &PyVertex::get_id)
    .def("add_neighbor", &PyVertex::add_neighbor)
    .def("remove_neighbor", &PyVertex::remove_neighbor)
    .def("neighbors", &PyVertex::neighbors)
    .def("get_value", &PyVertex::get_value)
    .def("set_value", &PyVertex::set_value)
    .def("get_outvalue", &PyVertex::get_outvalue)
    .def("set_outvalue", &PyVertex::set_outvalue)
    .def_readonly("name", &PyVertex::name)
    .def_readonly("connected_to", &PyVertex::connected_to)
    .def_readonly("value", &PyVertex::value)
    ;
    
    ///Wrap PyGraph
    ///////////////
    bp::class_<PyGraph>("PyGraph", bp::init<std::string>())
    .def("adjacent", &PyGraph::adjacent)
    .def("neighbors", &PyGraph::neighbors)
    .def("add_vertex", &PyGraph::add_vertex)
    .def("remove_vertex", &PyGraph::remove_vertex)
    .def("add_edge", &PyGraph::add_edge)
    .def("remove_edge", &PyGraph::remove_edge)
    .def("get_vertex_value", &PyGraph::get_vertex_value)
    .def("set_vertex_value", &PyGraph::set_vertex_value)
    .def("get_edge_value", &PyGraph::get_edge_value)
    .def("set_edge_value", &PyGraph::set_edge_value)
    .def("simulate", &PyGraph::simulate, simulate_overloads((bp::arg("tstart"),bp::arg("tstop"),bp::arg("tout"),bp::arg("solver")=0)))
    .def_readonly("name", &PyGraph::name)
    .def_readonly("vertex_list", &PyGraph::vertex_list)
    .def_readonly("num_vertices", &PyGraph::num_vertices)
    .add_property("rel_tolerance", &PyGraph::get_rel_tolerance, &PyGraph::set_rel_tolerance)
    .add_property("abs_tolerance", &PyGraph::get_abs_tolerance, &PyGraph::set_abs_tolerance)
    ;
}

/*
 * Simulate CCSS_MODEL instance generated from PyGraph
 */

void PyGraph::simulate(double tstart, double tstop, double tout, bool solver)
{
    std::cout << "Compiling function library..." << std::endl;
    int compile = PyRun_SimpleString("Function.Compile()");
    if(compile != 0){
        return;
    }
    std::cout << "Simulating..." << std::endl;
    
    ///Here we load dynamic library containing user-defined functions
    char path[] = "./functionlib.so";
    void* handle = dlopen(path, RTLD_LAZY); //open the function library
    if(!handle){ //if library not found
        std::cerr << "Cannot open library: " << dlerror() << std::endl;
        return;
    }
    
    ///Here we generate data to initialize CCSS_MODEL
    Graph network;
    std::vector<double> initial_conditions; //initial conditions
    unsigned int position = 0; //start position
    if(num_vertices == 0){
        std::cout << "PyGraph instance" << name << " has no vertices!" << std::endl;
        return;
    }
    for(unsigned int v=0; v<num_vertices; v++){ //loop over vertices
        std::string vertex_id = bp::extract<std::string>(vertex_list.keys()[v]); //get vertex name
        Vertex vertex; //create vertex
        VertexValue vertex_value = get_vertex_value(vertex_id); //get copy of vertex value
        vertex.name = vertex_id; //set vertex name
        int dimension = vertex_value.get_dimension(); //get copy of vertex dimension
        if(dimension == 0){ //check vertex dimension > 0
            std::cout << "PyVertex instance " << vertex_id << " has dimension zero. Is function set?" << std::endl;
            return; //if not break out of simulate
        }
        vertex.dimension = dimension;
        vertex.position = position; //set vertex position
        vertex.parameters = vertex_value.get_parameters(); //set vertex parameters
        VertexFunction vertex_function = (VertexFunction) dlsym(handle, vertex_value.function_id.c_str()); //extract and set symbol for vertex function
        if(dlerror()){ //if symbol not present
            std::cout << "Can not find function: " + vertex_value.function_id << std::endl;
            return; //break out of simulate
        }
        vertex.vertex_function = vertex_function; //set vertex function
        std::vector<double> init_conds = vertex_value.get_init_conds(); //get copy of initial conditions
        for(unsigned int i=0; i<dimension; i++){ //loop over vertex variables
            initial_conditions.push_back(init_conds[i]); //add initial conditions
        }
        bp::list neighbor_list = neighbors(vertex_id); //get copy of neighbor list for vertex
        int num_neighbors = len(neighbor_list); //get number of neighbors for vertex
        for(unsigned int u=0; u<num_neighbors; u++){ //loop over neighbors for vertex
            std::string neighbor_id = bp::extract<std::string>(neighbor_list[u]); //get neighbor name
            vertex.neighbor_list.push_back(vertex_list.keys().index(neighbor_id)); //add index of neighbor
            EdgeValue outedge_value = get_edge_value(vertex_id, neighbor_id); //get copy of outedge value
            vertex.edge_parameters.push_back(outedge_value.get_parameters()); //add copy of edge parameters
            EdgeFunction edge_function = (EdgeFunction) dlsym(handle, outedge_value.function_id.c_str()); //extract symbol for edge function
            if(dlerror()){ //if symbol not present
                std::cout << "Can not find function: " + outedge_value.function_id << std::endl;
                return; //break out of simulate
            }
            vertex.edge_functions.push_back(edge_function); //add outedge function
        }
        network.vertex_list.push_back(vertex); //add vertex to network
        position += vertex_value.get_dimension(); //increment position
    }

    ///Here we initialize CCSS_MODEL then perform simulation
    CCSS_MODEL model(network, position, initial_conditions, tstart, tstop, tout); //init model
    mkdir("results", ACCESSPERMS); //create results directory
    std::ofstream outputfile; //init output file
    outputfile.open("results/" + name + ".txt"); //open output file
    clock_t t1, t2; //time tracking
    t1 = clock(); //set start time
    ::simulate(model,outputfile,rel_tolerance,abs_tolerance,solver); //run simulation of model
    t2 = clock(); //set end time
    std::cout << std::endl << "Total simulation time was " << ((float)t2-(float)t1)/CLOCKS_PER_SEC << " seconds" << std::endl << std::endl;
    dlclose(handle); //close function library
    outputfile.close(); //close output file
    
    ///Here we copy results to one file per vertex
    mkdir("results/vertices", ACCESSPERMS); //create file for vertex output files
    for(unsigned int v=0; v<num_vertices; v++){ //loop over vertices
        Vertex vertex = network.vertex_list[v]; //get copy of vertex
        std::string vertex_id = vertex.name; //get name of vertex copy
        std::ofstream vertexfile; //init vertexfile
        vertexfile.open("results/vertices/" + vertex_id + ".txt"); //open new (overwrite old) vertexfile
    }
    std::string line; //init line
    std::ifstream inputfile("results/" + name + ".txt"); //open results file
    if(inputfile.is_open()){ //check open
        while(getline(inputfile,line)){ //get line of results file
            std::istringstream sub_line(line); //split line into words
            std::string time; //init time value
            sub_line >> time; //set time as first word
            for(unsigned int v=0; v<num_vertices; v++){ //loop over vertices
                Vertex vertex = network.vertex_list[v]; //get copy of vertex
                std::string vertex_id = vertex.name; //get name of vertex copy
                int dimension = vertex.dimension; //get dimension of vertex copy
                std::ofstream vertexfile; //init vertexfile
                vertexfile.open("results/vertices/" + vertex_id + ".txt", std::ofstream::app); //open vertex file to append
                vertexfile << time << " "; //append time
                for(unsigned int i=0; i<dimension; i++){ //loop over vertex variables
                    std::string variable; //init variable value
                    sub_line >> variable; //set variable as next word
                    vertexfile << variable << " "; //append variable to vertexfile
                }
                vertexfile << std::endl; //move to next line in vertexfile
            }
        }
    }
    
    ///Clear memory of CCSS_MODEL
    model.clear(); //clear model memory
    std::cout << std::endl;
}
