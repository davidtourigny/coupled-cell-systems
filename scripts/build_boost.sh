#!/usr/bin/env bash

# Copyright (C) 2018, 2019 Columbia University Irving Medical Center,
#     New York, USA

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

set -eux

# Use a default of one core.
: "${NPROC:=1}"

boost_underscore="${BOOST_VERSION//\./_}"

# Download the specified version.
curl -L -O "https://dl.bintray.com/boostorg/release/${BOOST_VERSION}/source/boost_${boost_underscore}.tar.gz"

# Unpack and install BOOST.
tar -xzf "boost_${boost_underscore}.tar.gz"
cd "boost_${boost_underscore}"

./bootstrap.sh --with-python=$(which python3)
./b2 -j "${NPROC}" --with-python install
ldconfig
rm project-config.jam

# Build graph-parallel using mpi
#./bootstrap.sh
#echo -e "using mpi ;\n$(cat project-config.jam)" > project-config.jam
#./b2 -j "${NPROC}" --with-graph_parallel --with-system install
#ldconfig

cd ..
rm -rf "boost_${boost_underscore}"*
