#!/usr/bin/env bash

# Copyright (C) 2018, 2019 Columbia University Irving Medical Center,
#     New York, USA

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

set -eux

# Use a default of one core.
: "${NPROC:=1}"

mpi_underscore="${MPI_VERSION}.0"

# Download the specified version.
curl -L -O "https://download.open-mpi.org/release/open-mpi/v${MPI_VERSION}/openmpi-${mpi_underscore}.tar.gz"

# Unpack and install GLPK.
tar -xzf "openmpi-${mpi_underscore}.tar.gz"
cd "openmpi-${mpi_underscore}"
./configure --prefix=/usr/local
make -j "${NPROC}" install
cd ..
rm -rf "openmpi-${mpi_underscore}"*
