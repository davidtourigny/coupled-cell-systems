## Build scripts

This directory contains the following content

* [`./build_boost.sh`](./build_boost.sh): script for downloading, building and
  installing specified Boost.Python version
* [`./build_ompi.sh`](./build_ompi.sh): script for for downloading, building and
installing specified Open MPI version
* [`./build_sundials.sh`](./build_sundials.sh): script for downloading, building
  and installing specified SUNDIALS version
* [`./README.md`](./README.md): this document

