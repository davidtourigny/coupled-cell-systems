## CMake modules

This directory contains the following content:

* [`./FindSUNDIALS.cmake`](./FindSUNDIALS.cmake): adapted from [casadi/casadi](https://github.com/casadi/casadi/blob/master/cmake/FindSUNDIALS.cmake)
* [`./README.md`](./README.md): this document

