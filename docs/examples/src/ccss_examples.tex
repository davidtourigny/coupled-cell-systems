\documentclass[fleqn,12pt]{wlscirep_no_abs}
\graphicspath{{.}{eps_figs/}}

\usepackage{trfsigns,amsmath,cleveref}
\usepackage{xcolor} 
\usepackage{placeins}
\usepackage{newtxtext}
\usepackage{newtxmath}
\renewcommand{\thefigure}{\arabic{figure}}
\usepackage{subcaption}

\usepackage{xr}

\newcounter{defcounter}
\setcounter{defcounter}{0}

\newenvironment{myequation}{%
\refstepcounter{defcounter}
\renewcommand\theequation{\thedefcounter}
\begin{equation}}
{\end{equation}}

\renewcommand{\thetable}{\arabic{table}}

% \usepackage[urlcolor=blue]{hyperref}      % links to citations
  \hypersetup{
      colorlinks = true,                    % text and not border
      citecolor = {blue},
      linkcolor = {purple},
             }

\definecolor{darkGreen}{rgb}{0.1,0.4,0.1}
\definecolor{veryLightGrey}{rgb}{0.95,0.95,0.95}

%  fully justified figures:
\captionsetup[figure]{justification=justified, singlelinecheck=off} 
%---------------------------------
%---------------------------------
\title{Example Scripts for {\em CCSS}}
\author[1]{David S. Tourigny}
\affil[1]{Columbia University Irving Medical Center, 630 W 168th St, New York, NY 10032, USA}

%\keywords{Keyword1, Keyword2, Keyword3}

%---------------------------------
\begin{document}
\flushbottom
\maketitle
\thispagestyle{empty}
%---------------------------------

\FloatBarrier  
 
\noindent This document is intended to serve as a companion document to the User Documentation for {\em CCSS}. It provides details, with listings, on the example scripts supplied with the {\em CCSS} distribution package.
\\

\noindent The following list summarises some of the examples supplied in the directory \texttt{examples} distributed with {\em CCSS}:

\begin{itemize}
\item \texttt{example\_1} builds a coupled cell network without edges containing two isomorphic cells and simulates the associated heterogenous {\em uncoupled} cell system by assigning different parameter values and initial conditions to each cell
\item \texttt{example\_2} builds a coupled cell network containing two identical cells with an identical pair of edges joining them in both directions. Being identical, the same vertex function and parameter values are associated with each cell, but these are assigned different initial conditions for simulation of the resulting homogenous coupled cell system
\item \texttt{example\_3} builds a coupled cell network containing a pair of non-isomorphic cells and a pair of non-isomorphic edges joining them in both directions. The associated coupled cell system is simulated 
\item  \texttt{example\_4} generates a randomised, heterogenous coupled cell network of $N$ FitzHugh-Nagumo neurons with diffusively-coupled edges. Parameter values including those for edge weights and neurons are also randomised in the simulated coupled cell system
\end{itemize}

\noindent In the following sections, we give detailed descriptions of these examples. Also presented are some sample plots generated using the $R$ script \texttt{plot.r} provided with each example, but users should be cautioned that their results may differ slightly from these. Descriptions for examples 1,2, and 4 are stand-alone meaning that, although examples are ordered in increasing complexity, the reader can jump directly to one of those sections should they so desire. The description of the third example assumes some familiarity with examples 1 and 2 however, and it is therefore recommended that these are treated as prerequisites for example 3. Additional examples distributed with {\em CCSS} are referenced below, but not described at length.

\section*{Example 1}

\noindent This first example builds and simulates an {\em uncoupled} cell system in order to provide a very simple entry point for users to get familiar with some basic capabilities of the program. The one-dimensional dynamical system associated with each of the two vertices in the network is
\begin{myequation}
 \dot{x} = -a \cdot x , 
\label{eqn:exp}
\end{myequation}
where $a>0$ is a parameter. Uncoupled implies that no edges are contained in the full system, which is given by
\begin{myequation}
 \dot{x}_1 = -a_1 \cdot x_1 , \quad  \dot{x}_2 = -a_2 \cdot x_2 ,
\label{eqn:expnet}
\end{myequation}
where values of $a_1 = 0.1$ and $a_2 = 0.2$ are used along with initial conditions $x_1(0) = 10.0$ and $x_2(0)=15.0$. This system is simple enough to be solved exactly, and its solution is $x_1(t) = 10.0 \cdot \exp(- a_1 \cdot t)$, $x_2(t) = 15.0 \cdot \exp(-a_2  \cdot t)$, but the remainder of this section will describe how the model is defined and simulated in the script \texttt{example1.py}. 
\\

\noindent The first two lines of \texttt{example1.py} import from the modules \texttt{ccss.network} and \texttt{ccss.functions} the classes required to build and simulate this coupled cell system using {\em CCSS}. A vertex function for the system is then defined by instantiating an object of the class \texttt{VertexFunction} with the name \texttt{"function"}, specifying that it will have dimension \texttt{1} and number of parameters \texttt{1}. A {\em Python} list containing variables (including time as the first entry in the list), and another containing parameters, to be used for symbolic definition of the right-hand side expression of the system in (\ref{eqn:exp}), are generated using the \texttt{VertexFunction} methods \texttt{get\_vars()} and \texttt{get\_params()}, respectively.  Subsequently, the expression for the derivative of the single dynamic variable (in this case just \texttt{1} corresponding to the single variable $x$) is set using these symbolic values through the \texttt{VertexFunction} method \texttt{set\_expression()}. This completes the definition of all functions for the uncoupled cell system in (\ref{eqn:expnet}).  
\\

\noindent Following its definition, the \texttt{VertexFunction} object is associated with an object of the class \texttt{VertexValue}: a \texttt{VertexValue} object is first instantiated and the \texttt{VertexFunction} object is assigned to it using the \texttt{VertexValue} method \texttt{set\_function()}. Since cells are non-identical in this example the parameter values will be different for either vertex in the network and initial conditions are also, but cells are isomorphic meaning the same \texttt{VertexFunction} object can be set prior to vertex creation. Creating a single instance of the \texttt{VertexValue} class that can then be modified prior to association with a vertex is often a much more efficient way to assign vertex values that avoids the requirement for re-instantiation of a new \texttt{VertexValue} object from scratch each time a new vertex is to be added. Once the vertex function has been assigned to a vertex value, a network with the name \texttt{"example1"} is instantiated as an object of the class \texttt{PyGraph}. The first vertex by the name of \texttt{"v"} is then added to the network using the \texttt{PyGraph} method \texttt{add\_vertex()}. Corresponding initial conditions $x_1(0) = 10.0$ and parameter value $a_1 = 0.1$ are set to the \texttt{VertexValue} object using methods \texttt{set\_init\_conds()} and \texttt{set\_params()}, respectively, and the \texttt{VertexValue} object is assigned to the vertex \texttt{"v"} using the \texttt{PyGraph} method \texttt{set\_vertex\_value()}. Finally, a second vertex by the name of \texttt{"u"} is added to the network by repeating the procedure using initial conditions $x_2(0) = 15.0$ and parameter value $a_2 = 0.2$. This completes the definition of the uncoupled cell system in (\ref{eqn:expnet}).      
\\

\noindent The simulation is executed via a call to the \texttt{PyGraph} method \texttt{simulate()} with a specified integration range of \texttt{tstart} $= 0.0$ to \texttt{tstop} $= 50.0$ and an output for plotting purposes every \texttt{tout} $= 0.1$. Figure \ref{fig:expnet} displays a plot of the simulation of this uncoupled cell system.    

\begin{figure}
\centering
\caption{Plot of the output of the simulation of the uncoupled cell system defined in example 1.}\label{fig:expnet}
\includegraphics[width=0.4\linewidth]{expnet.pdf}
\end{figure}

\section*{Example 2}

\noindent This example builds and simulates a simple coupled cell system composed of two identical oscillatory cells that are coupled diffusively. The resulting coupled cell system is inspired by the work of \cite{Tourigny17} in that the underlying two-dimensional dynamical system at each vertex is a perturbed Hamiltonian system of the form
\begin{myequation}
 \dot{w} = a \cdot z - \mu \cdot w , \quad \dot{z} = w^2 - 1.0 - \mu \cdot z,
\label{eqn:ham}
\end{myequation}
with $\mu << 1$ but non-zero because of complications that might arise should the solver be forced to deal with a system better suited to symplectic integration. Cells are diffusively coupled in both directions through both variables so that the resulting coupled cell system is of the form
\begin{myequation}
 \dot{w}_i = a_i \cdot z_i - \mu_i \cdot w_i + D_w (w_j - w_i), \quad \dot{z}_i = w^2_i - 1.0 - \mu_i \cdot z_i + D_z (z_j - z_i),
\label{eqn:hamnet}
\end{myequation}
for $i,j = 1,2$, $j \neq i$, with subscripts on the parameters $a_i$ and $\mu_i$ implying that, although isomorphic, in general cells may be non-identical. The version of (\ref{eqn:hamnet}) constructed in \texttt{example2.py} is a homogenous coupled cell system however, with $a_1 = a_2 = 1.0$ and $\mu_1 = \mu_2 = 0.001$. The diffusion coefficients are also equal, $D_w = D_z = 0.1$, although in principle need not be, and the only value attribute differing between vertices are the initial conditions $(w_1,z_1)(0) = (0.0, 0.0)$, $(w_2,z_2)(0) = (0.9,0.0)$. Readers should be aware that the $w,z$ notation used to define the coupled cell system here is {\em not} a substitute for the \texttt{x,y} notation used to set the \texttt{expression} attribute of the \texttt{VertexFunction} and \texttt{EdgeFunction} classes in the script \texttt{example2.py}. Hopefully this becomes clear from the discussion below. 
\\

\noindent Following import of the {\em CCSS} classes from \texttt{ccss.network} and \texttt{ccss.functions}, an object of the class \texttt{VertexFunction} is instantiated with the name \texttt{"vertex\_function"}, specifying that it will have dimension \texttt{2} and number of parameters \texttt{2}. A {\em Python} list containing variables (including time as the first entry in the list), and another containing parameters, to be used for symbolic definition of the right-hand side expression of the dynamical system in (\ref{eqn:ham}), are generated using the \texttt{VertexFunction} methods \texttt{get\_vars()} and \texttt{get\_params()}, respectively.  Subsequently, the expressions for the derivative of each dynamic variable in the two-dimensional system (\ref{eqn:ham}) (in this case \texttt{1,2} corresponding to $w,z$, respectively) are set using these symbolic values through the \texttt{VertexFunction} method \texttt{set\_expression()}. This completes the definition of the vertex function for the coupled cell system (\ref{eqn:hamnet}). Next, an object of the class \texttt{EdgeFunction} is instantiated with the name \texttt{"edge\_function"}. Here it is specified that \texttt{"edge\_function"} will be associated with edges originating from an output cell assigned a dynamical system of dimension \texttt{2} and extending to input cells also assigned a dynamical system of dimension \texttt{2}, with the total number of parameters in the coupling function being \texttt{2}. A list of variables (including time as the first entry in the list) for the input cell are generated using the \texttt{EdgeFunction} method \texttt{get\_in\_vars()}, those for the output cell using \texttt{get\_out\_vars()}, and a list of parameters are generated using the \texttt{EdgeFunction} method \texttt{get\_params()}. These symbolic values are then used to set the expressions for the diffusive coupling received at the input cell through the \texttt{EdgeFunction} method \texttt{set\_expression()}. This concludes the creation of functions required for generating the coupled cell system in (\ref{eqn:hamnet}). 
\\

\noindent Following the definition of vertex and edge functions, the constant attributes of vertex values and edge values (i.e., those attributes that are the same for all vertices or edges in the network) are defined by instantiating an object of the class \texttt{VertexValue} and \texttt{EdgeValue} respectively, and setting these attributes accordingly. Since the coupled cell network is homogenous in this example it is only the initial conditions that vary from vertex-to-vertex across the network, and therefore both functions and parameter values can be set prior to vertex/edge creation using the \texttt{VertexValue}/\texttt{EdgeValue} method \texttt{set\_params()}. Creating a single instance of each class that can then be modified with minimal effort immediately prior to association with an vertex/edge is often a much more efficient way to assign vertex and edge values. It avoids the requirement for re-instantiation of a new \texttt{VertexValue}/\texttt{EdgeValue} object from scratch each time a new vertex/edge is to be added. Once the constant attributes of \texttt{VertexValue} and \texttt{EdgeValue} have been set using the methods \texttt{set\_function()} and \texttt{set\_params()}, a new network with the name \texttt{"example2"} is instantiated as an object of the class \texttt{PyGraph}. The first vertex by the name of \texttt{"v"} is then added to the network using the \texttt{PyGraph} method \texttt{add\_vertex()}. Corresponding initial conditions $(w_1,z_1)(0) = (0.0, 0.0)$ are set to the \texttt{VertexValue} object using method \texttt{set\_init\_conds()}, and the \texttt{VertexValue} object is assigned to the vertex \texttt{"v"} using the \texttt{PyGraph} method \texttt{set\_vertex\_value()}. A second vertex by the name of \texttt{"u"} is added to the network by repeating the procedure using initial conditions $(w_2,z_2)(0) = (0.9,0.0)$. Finally, edges joining the vertices \texttt{"v"} and \texttt{"u"} in both directions are added to the network using the \texttt{PyGraph} method \texttt{add\_edge()} and identical edge values are set using the \texttt{PyGraph} method \texttt{set\_edge\_value()} with the same \texttt{EdgeValue} object as an argument.
\\

\noindent The simulation is executed via a call to the \texttt{PyGraph} method \texttt{simulate()} with a specified integration range of \texttt{tstart} $= 0.0$ to \texttt{tstop} $= 50.0$ and an output for plotting purposes every \texttt{tout} $= 0.1$. The two panels in Figure \ref{fig:hamnet} display a pair of plots from the simulation of this coupled cell system, showing the dynamics of vertex \texttt{"v"} (Figure \ref{fig:hamneta}) and vertex \texttt{"u"} (Figure \ref{fig:hamnetb}) individually.

\begin{figure}
\centering
\caption{Plots of sample outputs from simulation of the coupled cell system defined in example 2.}\label{fig:hamnet}
\begin{subfigure}[t]{.4\textwidth}
\centering
\includegraphics[width=\linewidth]{hamneta.pdf}
\caption{Dynamics of vertex \texttt{"v"}}\label{fig:hamneta}
\end{subfigure}
%
\begin{subfigure}[t]{.4\textwidth}
\centering
\includegraphics[width=\linewidth]{hamnetb.pdf}
\caption{Dynamics of vertex \texttt{"u"}}\label{fig:hamnetb}
\end{subfigure}
\end{figure}

\section*{Example 3}
\noindent This example builds and simulates a coupled cell system composed of two non-isomorphic cells joined in both directions by a pair of, necessarily, non-isomorphic couplings. The complete system originates from coupling a copy of each of the dynamical systems from example 1 and example 2, and the reader is therefore strongly encouraged to first gain experience with those examples prior to this one. Some of the theory and concepts described in the preceding two sections will be assumed here for brevity. The coupled cell system is defined by coupling (\ref{eqn:exp}) to (\ref{eqn:ham}) in both directions using coupling parameters $c_1,c_2$ to yield the total system
\begin{myequation}
\dot{x} + a \cdot x + c_1 \cdot x \cdot (w + z) =0 , \quad \dot{w} + \mu \cdot w + c_2 \cdot x \cdot w = b \cdot z, \quad \dot{z} + 1.0 + \mu \cdot z + c_2 \cdot x \cdot z = w^2.
\label{eqn:mixnet}
\end{myequation}
The implementation contained within the script \texttt{example3.py} builds and simulates this coupled cell system with parameter values $a = 0.1$, $b= 1.0$, $\mu = 0.001$, $c_1 = c_2 = 0.01$, and initial conditions $x(0) = 10.0$, $w(0) = 0.0$, $z(0) = 0.0$.   
\\

\noindent Following import of the {\em CCSS} classes from \texttt{ccss.network} and \texttt{ccss.functions}, an object of the class \texttt{VertexFunction} is instantiated with the name \texttt{"function\_A"}, specifying that it will have dimension \texttt{2} and number of parameters \texttt{2}, and is created in exactly the same way as \texttt{"vertex\_function"} from example 2. A second \texttt{VertexFunction} is then instantiated with the name \texttt{"function\_B"}, specifying that it will have dimension \texttt{1} and number of parameters \texttt{1}, and is created in exactly the same way as \texttt{"function"} from example 1. Next, an object of the class \texttt{EdgeFunction} is instantiated with the name \texttt{"function\_AB"}. Here it is specified that \texttt{"function\_AB"} will be associated with edges originating from output cells assigned a dynamical system of dimension \texttt{2} and extending to input cells of dimension \texttt{1}, with the total number of parameters in the function being \texttt{1}. Symbolic values are then generated for variables (including time) and parameters and used to set the expressions for coupling received at the input cell through the \texttt{EdgeFunction} method \texttt{set\_expression()}. Lastly, another object of the class \texttt{EdgeFunction} is instantiated with the name \texttt{"function\_BA"}. This time it is specified that \texttt{"function\_BA"} will be associated with edges originating from output cells assigned a dynamical system of dimension \texttt{1} and extending to input cells of dimension \texttt{2}, with the total number of parameters in the function being \texttt{1}. Symbolic values are again generated for variables (including time) and parameters and used to set the expressions for coupling received at the input cell through the \texttt{EdgeFunction} method \texttt{set\_expression()}, which concludes the creation of functions required for generating the coupled cell system in (\ref{eqn:mixnet}).  
\\

\noindent Two objects of the class \texttt{VertexValue}, one for each \texttt{VertexFunction} object, are instantiated and the respective functions set using the \texttt{VertexValue} method \texttt{set\_function()}. At this point initial conditions and parameter values are also set accordingly using \texttt{VertexValue} methods \texttt{set\_init\_conds()} and \texttt{set\_params()}, respectively. Similarly, two objects of the class \texttt{EdgeValue}, one for each \texttt{EdgeFunction} object, are instantiated and the respective functions set using the \texttt{EdgeValue} method \texttt{set\_function()} and parameter values with \texttt{set\_params()}. A new network with the name \texttt{"example3"} is instantiated as an object of the class \texttt{PyGraph} and the first vertex with name \texttt{"A"} is added using \texttt{add\_vertex()} with its value being assigned using  \texttt{set\_vertex\_value()}. The same is done adding a second vertex with name \texttt{"B"}, and two edges (\texttt{"A"}, \texttt{"B"}) and (\texttt{"B"}, \texttt{"A"}), each being assigned their corresponding values. 
\\

\noindent The simulation is executed via a call to the \texttt{PyGraph} method \texttt{simulate()} with a specified integration range of \texttt{tstart} $= 0.0$ to \texttt{tstop} $= 50.0$ and an output for plotting purposes every \texttt{tout} $= 0.1$. The two panels in Figure \ref{fig:mixnet} display a pair of plots from the simulation of this coupled cell system, showing the dynamics of vertex \texttt{"A"} (Figure \ref{fig:mixneta}) and vertex \texttt{"B"} (Figure \ref{fig:mixnetb}) individually.  

\begin{figure}
\centering
\caption{Plots of sample outputs from simulation of the coupled cell system defined in example 3.}\label{fig:mixnet}
\begin{subfigure}[t]{.4\textwidth}
\centering
\includegraphics[width=\linewidth]{mixneta.pdf}
\caption{Dynamics of vertex \texttt{"A"}}\label{fig:mixneta}
\end{subfigure}
%
\begin{subfigure}[t]{.4\textwidth}
\centering
\includegraphics[width=\linewidth]{mixnetb.pdf}
\caption{Dynamics of vertex \texttt{"B"}}\label{fig:mixnetb}
\end{subfigure}
\end{figure}  

\section*{Example 4}

\noindent This example employs a model for a heterogenous FitzHugh-Nagumo neural network inspired by the work of \cite{Plotnikov16}. The FitzHugh-Nagumo cell model is defined at each vertex by the two-dimensional dynamical system
\begin{myequation}
\epsilon \dot{u} = u - \frac{u^3}{3} - v , \quad \dot{v} = u + a ,
\label{eqn:FHN}
\end{myequation}
where $\epsilon$ is a time-scale parameter (in \cite{Plotnikov16} and the script \texttt{example4.py} a value of $\epsilon = 0.01$ is used) and $a$ is a threshold parameter: for $|a|>1$ the FitzHugh-Nagumo model has a locally stable equilibrium point, whilst for $|a|<1$ there is a stable limit cycle due to a supercritical Hopf bifurcation at $|a| = 1$. The network is generated by diffusively coupling $N$ nodes with dynamics (\ref{eqn:FHN}) through the $u$ variable, resulting in the coupled cell system
\begin{myequation}
\epsilon \dot{u}_i = u_i - \frac{u^3_i}{3} - v_i + C \cdot \sum_{j=1}^N W_{ij} \cdot (u_j - u_i), \quad \dot{v}_i = u_i + a_i ,
\label{eqn:FHNN}
\end{myequation}
where $C$ is the overall coupling strength, $W_{ij}$ the relative weight of the coupling between the $i$,$j$th nodes, and each FitzHugh-Nagumo neuron has its own value of the parameter $a_i$. Implementation of this heterogenous coupled cell system in the script \texttt{example4.py} generates the value of $a_i$ for each of the $N$ neurons at random from a uniform distribution on the interval $[0,2]$. Edges are added symmetrically between neurons $i$ and $j$ with probability $1-p$ (default value $p = 0.75$) and a weight chosen at random from a uniform distribution on the interval $(0,1]$ (symmetrically meaning there is also an edge with the same weight from $j$ to $i$). The value of $C$ is set to $C=0.05$ and initial conditions $(u_i,v_i) = (0,0)$ $\forall i = 1,2,..., N$ are the same as those used in \cite{Plotnikov16}.
\\

\noindent Following import of the {\em CCSS} classes from \texttt{ccss.network} and \texttt{ccss.functions}, there is an additional import of the {\em Python} module \texttt{numpy} required for random number generation. A vertex function for the FitzHugh-Nagumo model (\ref{eqn:FHN}) is then defined by instantiating an object of the class \texttt{VertexFunction} with the name \texttt{"FitzHughNagumo"}, specifying that it will have dimension \texttt{2} and number of parameters \texttt{2}. A {\em Python} list containing variables (with time as the first entry), and another containing parameters, to be used for symbolic definition of the right-hand side expression of the FitzHugh-Nagumo model in (\ref{eqn:FHN}), are generated using the \texttt{VertexFunction} methods \texttt{get\_vars()} and \texttt{get\_params()}, respectively.  Subsequently, the expressions for the derivative of each dynamic variable in the FitzHugh-Nagumo model (in this case \texttt{1,2} corresponding to $u,v$, respectively) are set using these symbolic values through the \texttt{VertexFunction} method \texttt{set\_expression()}. This completes the definition of the vertex function for the FitzHugh-Nagumo cell model (\ref{eqn:FHN}). Similarly, an edge function with the name \texttt{"Coupling"} is instantiated as an object of the class \texttt{EdgeFunction}. Here it is specified that \texttt{"Coupling"} will be associated with edges originating from output  cells assigned a dynamical system of dimension \texttt{2} and extending to input cells also of dimension \texttt{2}, with the total number of parameters in the function being \texttt{1}. A list of variables (with time as the first entry) for the input cell are generated using the \texttt{EdgeFunction} method \texttt{get\_in\_vars()}, those for the output cell using \texttt{get\_out\_vars()}, and a list of parameters are generated using the \texttt{EdgeFunction} method \texttt{get\_params()}. These symbolic values are then used to set the expressions for the diffusive coupling received by the input cell through the \texttt{EdgeFunction} method \texttt{set\_expression()}. This completes the definition of the edge function for the FitzHugh-Nagumo network and concludes the creation of functions required for generating the entire coupled cell system in (\ref{eqn:FHNN}).  
\\

\noindent Following the definition of vertex and edge functions, the constant attributes of vertex values and edge values (i.e., those attributes that are the same for all vertices or edges in the network) are defined by instantiating an object of the class \texttt{VertexValue} and \texttt{EdgeValue} respectively, and setting these attributes accordingly. Since all cells are non-identical but isomorphic in this example it is only the parameter values that will vary from vertex-to-vertex or edge-to-edge across the network, and therefore both functions and initial conditions can be set prior to vertex/edge creation using the \texttt{VertexValue}/\texttt{EdgeValue} method \texttt{set\_function()} and, in the case of \texttt{VertexValue}, \texttt{set\_init\_conds()}. Creating a single instance of each class that can then be modified with minimal effort immediately prior to association with an vertex/edge is often a much more efficient way to assign vertex and edge values. It avoids the requirement for re-instantiation of a new \texttt{VertexValue}/\texttt{EdgeValue} object from scratch each time a new vertex/edge is to be added. Once the constant attributes of \texttt{VertexValue} and \texttt{EdgeValue} have been set, a new network with the name \texttt{"example4"} is instantiated as an object of the class \texttt{PyGraph}. The global model parameters $\epsilon=$ time-scale parameter, $N=$ number of vertices, $C=$ total coupling strength, and $p=$ edge absence probability are then defined. $N$ (default value $10$) vertices are successively added to the network using the \texttt{PyGraph} method \texttt{add\_vertex()}, each with a random choice of the parameter $a$ first set to the \texttt{VertexValue} object using \texttt{set\_params()}, and then added to the network using the \texttt{PyGraph} method \texttt{set\_vertex\_value()}. Next, edges are added to the network with probability $1-p$, first setting the new edge weight to the master \texttt{EdgeValue} object, and then to the network using the \texttt{PyGraph} method \texttt{set\_edge\_value()}. This method is called twice, once for each ordering of the vertex pair to achieve a symmetric network arrangement.          
\\

\noindent The simulation is executed via a call to the \texttt{PyGraph} method \texttt{simulate()} with a specified integration range of \texttt{tstart} $= 0.0$ to \texttt{tstop} $= 50.0$ and an output for plotting purposes every \texttt{tout} $= 0.1$. The two panels in Figure \ref{fig:FHNN} display a selected pair of plots from a simulation of this coupled cell system with $C=0.05$, showing one FitzHugh-Nagumo neuron in the model with a stable limit cycle (Figure \ref{fig:FHNNa}) and the other with a stable equilibrium (Figure \ref{fig:FHNNb}). In this particular case, coupling strength and the random network topology were not sufficient to alter the intrinsic FitzHugh-Nagumo dynamics of these cells in the model. When the coupling strength $C$ is increased to $0.5$ however, the dynamics of some individual cells may departure from those of the underlying FitzHugh-Nagumo model.         

\begin{figure}
\centering
\caption{Plots of sample outputs from simulation of the coupled cell system defined in example 4 with $C=0.05$.}\label{fig:FHNN}
\begin{subfigure}[t]{.4\textwidth}
\centering
\includegraphics[width=\linewidth]{FHNNa.pdf}
        \caption{Neuron with stable limit cycle}\label{fig:FHNNa}
\end{subfigure}
%
\begin{subfigure}[t]{.4\textwidth}
\centering
\includegraphics[width=\linewidth]{FHNNb.pdf}
\caption{Neuron with stable equilibrium point}\label{fig:FHNNb}
\end{subfigure}
\end{figure}
% 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\bibliographystyle{plain}

\begin{thebibliography}{99}
\bibitem{Tourigny17} Tourigny DS (2017) Networks of planar Hamiltonian systems. {\em Comm. Nonlin. Sci. Numer. Simul.} 53: 263- 277
\bibitem{Plotnikov16} Plotnikov SA, Lehnert J, Fradkov AL, \& Sc\"{o}ll E (2016) Synchronization in heterogenous FitzHugh-Nagumo networks with hierarchical architecture. {\em Phys. Rev.} E 94: 012203
\end{thebibliography}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{document}
