## Documentation

This directory contains a [user guide](./guide/ccss_guide.pdf) in addition to documentation [ccss_examples](./examples/ccss_examples.pdf) for the example scripts supplied with this repository.

