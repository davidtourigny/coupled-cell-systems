\documentclass[fleqn,12pt]{wlscirep_no_abs}
\graphicspath{{.}{eps_figs/}}

\usepackage{trfsigns,amsmath,cleveref}
\usepackage{xcolor} 
\usepackage{placeins}
\usepackage{newtxtext}
\usepackage{newtxmath}
\renewcommand{\thefigure}{\arabic{figure}}
\usepackage{subcaption}
\usepackage{listings}
\usepackage{color}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\usepackage{xr}

\newcounter{defcounter}
\setcounter{defcounter}{0}

\newenvironment{myequation}{%
\refstepcounter{defcounter}
\renewcommand\theequation{\thedefcounter}
\begin{equation}}
{\end{equation}}

\renewcommand{\thetable}{\arabic{table}}

% \usepackage[urlcolor=blue]{hyperref}      % links to citations
  \hypersetup{
      colorlinks = true,                    % text and not border
      citecolor = {blue},
      linkcolor = {purple},
             }

\definecolor{darkGreen}{rgb}{0.1,0.4,0.1}
\definecolor{veryLightGrey}{rgb}{0.95,0.95,0.95}

\lstset{
  language=Python,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}

%  fully justified figures:
\captionsetup[figure]{justification=justified, singlelinecheck=off} 
%---------------------------------
%---------------------------------
\title{User Documentation for {\em CCSS}}
\author[1]{David S. Tourigny}
\affil[1]{Columbia University Irving Medical Center, 630 W 168th St, New York, NY 10032, USA}

%\keywords{Keyword1, Keyword2, Keyword3}

%---------------------------------
\begin{document}
\flushbottom
\maketitle
\thispagestyle{empty}
%---------------------------------

\tableofcontents
\newpage

\FloatBarrier  
\section{Introduction}

\subsection{Overview of the package}

{\em CCSS} is a work-in-progress project toward a freely available software package that provides an efficient and accessible platform for building, manipulating, and simulating coupled cell systems. {\em CCSS} is designed in an object-oriented fashion, implemented using a combination of {\em C++} and {\em Python} in order to meet the performance demands of numerically simulating large dynamical systems while retaining a user-interface based on an intuitive scripting language. Simulations are performed using {\em SUNDIALS CVODE} (\url{https://computation.llnl.gov/projects/sundials/cvode}), a robust solver with variable-order, variable-step multistep methods that can effectively integrate both stiff and non-stiff ordinary differential equations (ODEs). To avoid the user having to interact with the lower-level programming language directly, {\em CCSS} modules have been exposed to {\em Python} to conveniently build and simulate a wide range of coupled cell systems.

The current implementation is intended only to capture structural aspects of the user interface meaning that numerical integration algorithms have not yet been optimised for speed. Future implementations will take advantage of the dev capabilities of {\em SUNDIALS} version 5.0.0-dev.1 and the {\em Parallel Boost Graph Library} (\url{https://www.boost.org/doc/libs/1_71_0/libs/graph_parallel/doc/html/index.html}) to leverage the inherent network structure of coupled cell systems for parallelisation. Parallelisation will be based on {\em Open MPI} (\url{https://www.open-mpi.org/}) and the option to incorporate MPI test cases into the build process is therefore supplied with this repository.                  

\subsection{Release licence}
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program. If not, see \url{http://www.gnu.org/licenses/}. Copyright (C) David S. Tourigny 2018, 2019 Columbia University Irving Medical Center, New York, USA. 

\section{Mathematical considerations}
This section provides a mathematical overview of the dynamical systems that {\em CCSS} is concerned with building and simulating, namely, coupled cell systems as defined by Stewart, Golubitsky and colleagues \cite{Golubitsky02,Stewart03,Golubitsky06}. A range of applications are also briefly discussed.

\subsection{Coupled cell systems}
The term {\em cell} refers to a system of ODEs whereas a {\em coupled cell system} is a set of $N$ cells coupled together. That is to say, a coupled cell system is a dynamical system whose variables correspond to those of a collection of cells, such that the output of one cell can affect the dynamics of the others that it is coupled to. Underlying the definition of a coupled cell system is a {\em coupled cell network}, which is a (labeled) directed graph that specifies the system architecture, i.e. which cells are coupled to which, the type of dynamics within each cell, and the nature of the coupling between each pair. In a coupled cell network each cell is represented by a vertex carrying a reference to the ODE describing its dynamics, the parameter values associated with that system, and a set of initial conditions for simulation of the associated coupled cell system. Throughout this document the terminology {\em vertex} and {\em directed graph} will be used interchangeably with {\em cell} and {\em coupled cell network}, but strictly speaking a cell or coupled cell network refer to a vertex or directed graph {\em with} these additional data labels. 
\\

\noindent Two cells in a coupled cell network are considered {\em isomorphic} if their individual dynamics are governed by the same differential equations. Being isomorphic implies two individual cells have the same phase space but, since their dynamics might depend on certain parameters, isomorphic cells are only considered {\em identical} if these parameter values are the same for both cells also. The trajectories of two identical cells may be different in the simulation of the associated coupled cell system if, for example, initial conditions are not the same, but this has no bearing on the definition of identical cells from the perspective of the coupled cell network. If all cells in the network are identical then the network is said to be {\em homogenous}, otherwise the coupled cell network is {\em heterogenous}. For a coupled cell network containing $N$ cells with dynamics $\{ \dot{x}_i(t) = f_i(t, x_i(t)) \}_{i=1,...,N}$ of dimensions $\{n_i\}_{i=1,...,N}$, respectively, the total phase space is 
\begin{equation}
\label{phase}
P = \{ (t,x): t\in \mathbb{R} \quad x = (x_1,...,x_N) \in \mathbb{R}^{n_1} \times \cdots \times \mathbb{R}^{n_N} \} . 
\end{equation}          
In general the function $f_i$ may also depend on time $t$ and $k_i$ parameter values $p^i = (p^i_0, ...,p^i_{k_i-1})$ and simulation requires knowledge of initial conditions $\{ x_i(0) \}_{i= 1,...,N}$.
\\

\noindent {\em Couplings} between cells are introduced by adding edges between vertices representing those cells in the directed graph. Formally, a directed graph $G$ consists of a vertex set $V(G)$ and an edge set $E(G)$, where an edge is an ordered pair of distinct vertices. Being directed, an edge $(v,u) \in E(G)$ originating from a vertex $v \in V(G)$ and ending at another $u \in V(G)$ need not imply that there exists an edge in the opposite direction, $(u,v)$, in the edge set $E(G)$. This is only the case for symmetric (undirected) graphs. The cell represented by vertex $v \in V(G)$ in the edge $(v,u) \in E(G)$ is said to be the {\em output} cell (its output is passed by the edge) and that represented by $u \in V(G)$ the {\em input} cell (it receives input from the edge). The edges of a coupled cell network carry additional information in the form of a reference to the function defining the nature of the coupling and the values of parameters required to define it. As for cells, two couplings (each represented by an edge in the directed graph) are said to be {\em isomorphic} if their coupling functions are the same. Two isomorphic couplings are {\em identical} if their parameter values are equal also. In general, for a coupled cell network containing $M$ couplings, the coupling functions $\{ h_{\alpha}: \mathbb{R} \times \mathbb{R}^{n^{out}_{\alpha}} \times \mathbb{R}^{n^{in}_{\alpha}} \to \mathbb{R}^{n^{in}_{\alpha}}  \}_{{\alpha}=1,...,M}$, where $n^{in}_{\alpha}$ and $n^{out}_{\alpha}$ are the dimensions of the input and output cells, respectively, may depend on time $t$ and each $h_{\alpha}$ on $k_{\alpha}$ parameter values $q^{\alpha} = (q^{\alpha}_0, ...,q^{\alpha}_{k_{\alpha}-1})$. Using notation $C(i)$ to denote the set of cells that are joined to the $i$th by an edge in the coupled cell network and $\alpha(i,j)$ to denote the index of the coupling function for the edge $(j,i)$, the associated coupled cell system is given by 
\begin{equation}
\label{ccss}
\dot{x}_i(t) = f_i(t, x_i(t),p_i) + \sum_{j \in C(i)} h_{\alpha(i,j)}(t, x_i(t),x_j(t),q_j) .
\end{equation}
The precise trajectory that the dynamical system (\ref{ccss}) traces out on the total phase space (\ref{phase}) depends on the initial conditions $\{ x_i(0) \}_{i= 1,...,N}$. Many important results on coupled cell systems can be found in the seminal work \cite{Stewart03,Golubitsky06} and the hundreds of papers citing these, but solutions are usually intractable except in very special cases of particularly simple dynamics or a high-degree of symmetry. {\em CCSS} instead provides an efficient method for constructing and numerically simulating any coupled cell systems the user might wish to investigate.       

\subsection{Applications and examples}
Coupled cell systems generalise mathematical models for many real-world systems, including but not limited to: neural networks, coupled lasers, coupled mechanical systems, central pattern generators, and ecological speciation (see \cite{Golubitsky02} for references and a discussion of some of these). The \texttt{examples} directory distributed with {\em CCSS} contains four example coupled cell systems that include networks of diffusively coupled perturbed Hamiltonian systems \cite{Tourigny17} and a heterogenous network of randomly coupled FitzHugh-Nagumo neurons \cite{Plotnikov16}. See the accompanying Example Scripts for {\em CCSS} document for a discussion of these examples. 

\section{Getting started}

\subsection{Installing and running {\em CCSS}}

\subsubsection{Installation procedure}
{\em CCSS} runs on {\em Linux} and {\em OSX}. A Dockerfile is provided for building a {\em Docker} (\url{https://docs.docker.com/}) image to run the software from an interactive container. The image can be built in one step by issuing the command:
\begin{lstlisting}
make build
\end{lstlisting}
from the root of the repository. It can then be started using
\begin{lstlisting}
make run
\end{lstlisting}
Alternatively, {\em CCSS} can be built and installed directly from source once the dependencies (below) have been installed correctly. The package can be installed directly from source from the root of this repository by running:
\begin{lstlisting}
./scripts/build_ccss.sh
\end{lstlisting}
The modules are then installed to {\em Python} site packages using the command:
\begin{lstlisting}
pip install .
\end{lstlisting}

\subsubsection{Dependencies}
Building directly from source depends on having the following packages pre-installed on the user's platform:
\begin{itemize}
\item {\em CMake} (\url{https://cmake.org/}) version 2.8.3 or higher
\item {\em Boost} (\url{https://www.boost.org/}) version 1.68.0
\item {\em Python} (\url{https://www.python.org/}) version 3.6 or higher with {\em SymPy} (\url{https://www.sympy.org/en/index.html}) installed
\item {\em SUNDIALS} (\url{https://computation.llnl.gov/projects/sundials}) version 5.0.0-dev.1
\item a suitable {\em C++} compiler, e.g. {\em GCC} (\url{http://gcc.gnu.org/})
\end{itemize} 
{\em Boost} and {\em SUNDIALS} are most conveniently obtained, built and installed running the scripts supplied with the repository:
\begin{lstlisting}
./scripts/build_boost.sh
\end{lstlisting}
\begin{lstlisting}
./scripts/build_sundials.sh
\end{lstlisting}
Version number variables should be exported prior to running these commands.

\subsubsection{Using the modules}
\noindent Once installed, the {\em CCSS} modules must be imported before they can be used in either {\em Python}. To import everything from both of them, one can do
     
\begin{lstlisting}
from ccss.network import *
from ccss.functions import *
\end{lstlisting}

\noindent In the following subsections, it will always be assumed that the previous two lines of code were run.

\subsection{Building topology for a coupled cell network}
An empty coupled cell network can be created by instantiating a \texttt{PyGraph} object
\begin{lstlisting}
network = PyGraph("example") # creates empty directed graph with name "example"
\end{lstlisting}
Once a directed graph has been created it can be populated with vertices (representing cells in the coupled cell network) using the \texttt{PyGraph} method \texttt{add\_vertex()}
\begin{lstlisting}
network.add_vertex("v") # adds vertex with name "v"
\end{lstlisting}
The method returns an object of the class \texttt{PyVertex} that serves as a reference to this vertex. If a second vertex is added
\begin{lstlisting}
u = network.add_vertex("u") # adds vertex with name "u"
\end{lstlisting}
then it is possible to also add an edge joining the two vertices in either direction using the \texttt{PyGraph} method \texttt{add\_edge()}. For example, 
\begin{lstlisting}
network.add_edge("v","u") # adds edge ("v","u")
\end{lstlisting}
adds an edge from vertex \texttt{"v"} to vertex \texttt{"u"}.  Vertices and edges can be removed using the \texttt{PyGraph} methods \texttt{remove\_vertex()} and \texttt{remove\_edge()}, respectively,
\begin{lstlisting}
network.remove_edge("v","u") # removes edge ("v","u") from network
network.remove_vertex("v") # removes vertex "v" from network
\end{lstlisting}
At any point the number of vertices present in the coupled cell network can be queried by accessing the \texttt{PyGraph} attribute \texttt{num\_vertices}
\begin{lstlisting}
network.num_vertices # number of vertices in network
\end{lstlisting}
or accessing vertices themselves through the \texttt{PyGraph} attribute \texttt{vertex\_list}, which is a \texttt{dict} containing all \texttt{PyVertex} objects in the network with their names as corresponding keys 
\begin{lstlisting}
network.vertex_list["v"] # PyVertex object with name "v"
\end{lstlisting} 
\texttt{PyGraph} contains two additional methods for querying the topology of a coupled cell network. The method \texttt{adjacent()} takes two vertex names as arguments and returns \texttt{True} if they are connected by an edge and \texttt{False} otherwise
\begin{lstlisting}
network.adjacent("v","u") # returns True if edge ("v","u") present in network
\end{lstlisting} 
The method \texttt{neighbors()} takes a vertex name as an argument and returns a \texttt{list} of neighboring vertex names
\begin{lstlisting}
network.neighbors("v") # returns list containing names of vertex "v"'s neighbors
\end{lstlisting}

\subsection{Building functions for a coupled cell network}
The functions to be associated with vertices and edges of the coupled cell network (specifying a cell's dynamical system and a coupling function, respectively) are defined as objects of the classes \texttt{VertexFunction} and \texttt{EdgeFunction}, respectively. A vertex function is created by instantiating a \texttt{VertexFunction} object with a given name, specifying the dimension of the dynamical system it will define and the number of parameter values that it depends on
\begin{lstlisting}
vf = VertexFunction("f", 2, 1) # vertex function with dimension 2 and 1 parameter value
\end{lstlisting}
Similarly, an edge function is created by instantiating an \texttt{EdgeFunction} object with a given name, specifying dimensions assigned to both the output and input cell and the number of parameter values that it depends on  
 \begin{lstlisting}
ef = EdgeFunction("h", 1, 2, 1) # edge function with input cell dimension 1, output cell dimension 2, and 1 parameter value
\end{lstlisting}
A vertex function is defined in full by first generating a list of symbolic values for variables (including time as the first entry of the list) and parameters using the \texttt{VertexFunction} methods \texttt{get\_vars()} and \texttt{get\_params()}, respectively,  
 \begin{lstlisting}
x = vf.get_vars() # get list of symbolic variable values for time and cell
p = vf.get_params() # get list of symbolic parameter values for cell
\end{lstlisting}
The symbolic expression for the derivative of each variable in the dynamical system is then set with the \texttt{VertexFunction} method \texttt{set\_expression()}
 \begin{lstlisting}
vf.set_expression(1, x[0]*x[1]*p[0]) # sets expression for derivative of first variable
vf.set_expression(2, x[2]*p[0]) # sets expression for derivative of second variable
\end{lstlisting}
In this example it should be clear that the resulting dynamical system is $\dot{x}_1(t) = p \cdot t \cdot x_1(t)$, $\dot{x}_2(t) = p \cdot x_2(t)$ where the $x_i(t)$ ($i=1,2$) are variables and $p$ is a parameter.
\\

\noindent An edge function is defined by generating a list of symbolic values for variables (including time as the first entry of the list) associated with the input cell and the output cell, and for parameters, using the \texttt{EdgeFunction} methods \texttt{get\_in\_vars()}, \texttt{get\_out\_vars()}, and \texttt{get\_params()}, respectively,  
 \begin{lstlisting}
x = ef.get_in_vars() # get list of symbolic variable values for time and input cell
y = ef.get_out_vars() # get list of symbolic variable values for time and output cell
p = ef.get_params() # get list of symbolic parameter values for coupling
\end{lstlisting}
Expressions for the coupling function are set using the \texttt{EdgeFunction} method \texttt{set\_expression()}
 \begin{lstlisting}
ef.set_expression(1, p[0]*x[0]*y[0]*x[1]*y[1]*y[2]) # sets coupling expression
\end{lstlisting}
In this example it should be clear that the resulting coupling function is $p \cdot t^2 \cdot x(t) \cdot y_1(t) \cdot y_2(t)$, where $y_i(t)$ ($i=1,2$) are variables of the dynamical system at the origin cell, $x(t)$ is the variable of the dynamical system at the receiving cell, and $p$ is a parameter.

\subsection{Assigning values to vertices and edges}
Values to be assigned to vertices and edges and complete the definition of cells and couplings are created as objects of the classes \texttt{VertexValue} and \texttt{EdgeValue}, respectively. A vertex value is created by instantiating a \texttt{VertexValue} object and an edge value by instantiating an \texttt{EdgeValue} object
 \begin{lstlisting}
v_val = VertexValue() # creates an empty vertex value
e_val = EdgeValue() # creates an empty edge value
\end{lstlisting}
Vertex values can be assigned an instance of the class \texttt{VertexFunction}, a list of parameter values for the corresponding vertex function, and a list of initial conditions using the appropriate \texttt{VertexValue} methods 
 \begin{lstlisting}
v_val.set_function(vf) # assigns VertexFunction object vf to v_val 
v_val.set_params([1.0]) # assigns parameter values [1.0] to v_val
v_val.set_init_conds([0.0,0.0]) # assigns initial conditions [0.0,0.0] to v_val
\end{lstlisting}
Here the length of the list supplied with \texttt{set\_params()} must be equal to the number of parameters in the vertex function associated with a \texttt{VertexValue} and the length of the list supplied with \texttt{set\_init\_conds()} must be equal to its dimension. 
\\

\noindent Similarly, edge values can be assigned an instance of the class \texttt{EdgeFunction} and a list of parameter values for the corresponding edge function using the appropriate \texttt{EdgeValue} methods 
 \begin{lstlisting}
e_val.set_function(ef) # assigns EdgeFunction object ef to e_val 
e_val.set_params([0.1]) # assigns parameter values [0.1] to e_val
\end{lstlisting}
Here the length of the list supplied with \texttt{set\_params()} must be equal to the number of parameters in the edge function associated with a \texttt{EdgeValue}. 
\\

\noindent After instantiating objects of the classes \texttt{VertexValue} and \texttt{EdgeValue} these can be assigned to vertices and edges using the \texttt{PyGraph} methods \texttt{set\_vertex\_value()} and \texttt{set\_edge\_value()}, respectively,    
 \begin{lstlisting}
network.set_vertex_value("v",v_val) # assigns v_val to vertex "v" in network
network.set_edge_value("v","u",e_val) # assigns e_val to edge ("v","u") in network
\end{lstlisting}
The class \texttt{PyGraph} also has methods for returning the values associated with a particular vertex or edge
 \begin{lstlisting}
network.get_vertex_value("v") # returns VertexValue object associated with "v"
network.get_edge_value("v","u") # returns EdgeValue object associated with ("v","u")
\end{lstlisting}
Objects of both classes \texttt{VertexValue} and \texttt{EdgeValue} contain an additional attribute \texttt{user\_data}, which allows arbitrary additional data to be associated with edges and vertices.  


\subsection{Simulating a coupled cell system}
Once a coupled cell network has been built, simulation of the associated coupled cell system is initiated using the \texttt{PyGraph} method \texttt{simulate()} while specifying the limits of integration and the spacing of outputs 
\begin{lstlisting}
network.simulate(0,10,0.1) # simulate interval [0,10.0] with outputs every 0.1
\end{lstlisting}
When first called, the \texttt{simulate()} method will invoke the \texttt{Function} class method \texttt{Compile()} to write and compile all \texttt{Function} objects defined in the current execution to the library \texttt{functionlib.so}. If unsuccessful this will result in an error, but otherwise the method will proceed to simulation. This involves dynamically loading the library, extracting the required functions, and assembling these with other data in a manner consistent for passing the coupled cell system to {\em SUNDIALS CVODE}. After simulation has completed the user will see a summary of the underlying {\em SUNDIALS CVODE} procedure, including how long the simulation took and whether or not it was successful.
\\

\noindent By default the the \texttt{PyGraph} method \texttt{simulate()} calls {\em SUNDIALS CVODE} with the linear multistep method \texttt{CV\_ADAMS} that is recommended for non-stiff problems. Should the user wish to override this default choice, an additional \texttt{simulate()} argument \texttt{solver} can be provided with the value \texttt{True} (default value \texttt{False})
\begin{lstlisting}
network.simulate(0,10,0.1,True) # simulate with CV_BDF multistep method
\end{lstlisting}
This will instead call  {\em SUNDIALS CVODE} with the linear multistep method \texttt{CV\_BDF}, which is recommended for stiff problems. The  \texttt{PyGraph}  tolerance attributes \texttt{rel\_tolerance} and \texttt{abs\_tolerance} corresponding to the {\em SUNDIALS CVODE} tolerances \texttt{reltol} and \texttt{abstol} (see {\em SUNDIALS CVODE} user documentation for details), respectively, can also be modified from their default values of $10^{-4}$
\begin{lstlisting}
network.rel_tolerance = 10e-6 # change rel_tolerance attribute to 10e-6
network.abs_tolerance = 10e-6 # change abs_tolerance attribute to 10e-6
\end{lstlisting}
Unfortunately, given the hierarchical nature of coupled cell systems, modifying the \texttt{abstol} for each {\em SUNDIALS CVODE} variable individually is not practical and can therefore only be done through the source code directly.    
\\

\noindent If simulation fails or an error is thrown the user should check very carefully whether their coupled cell system is well-defined, before perhaps concluding that it forms a special case that {\em SUNDIALS CVODE} finds hard to deal with under the default simulation settings. These can then be adjusted in an attempt to remedy the problem. Outputs of a successful simulation are written to a text file in the \texttt{results} directory and also a subdirectory \texttt{results/vertices} containing individual text files for each vertex in the network.  

 \section{Module documentation}
 This section contains a summary of the {\em CCSS} modules \texttt{network} and \texttt{functions}.
\subsection{\texttt{functions}}
The module \texttt{functions} provides
\begin{itemize}
\item A \texttt{Function} base class for function compilation
\item A \texttt{VertexFunction} derived class encapsulating vertex functions
\item An \texttt{EdgeFunction} derived class encapsulating edge functions
\item Symbolic definitions for mathematical functions
\item Variables for compilation and methods for writing library files
\end{itemize}
This module is implemented in {\em Python}.
\\

\noindent {\em class} \texttt{functions.\textbf{Function}(}{\em name}, {\em header}\texttt{)}
\\

\texttt{\textbf{write\_function}()} 
\\
\indent Writes expression representing \texttt{self} to \texttt{functionlib.cpp} 
\\

\texttt{\textbf{id}} 
\\
\indent Attribute storing {\em name} assigned to \texttt{self}
\\

\texttt{\textbf{header}} 
\\
\indent Attribute storing expression for {\em header} to be used for representation of \texttt{self} in \texttt{functionlib.cpp}
\\

\texttt{\textbf{GetInstances}()} 
\\
\indent Class method returning copy of class attribute \texttt{Instances}
\\

\texttt{\textbf{AddInstance}(}{\em instance}\texttt{)} 
\\
\indent Class method for adding {\em instance} to class attribute \texttt{Instances}
\\

\texttt{\textbf{RemoveInstance}(}{\em instance}\texttt{)} 
\\
\indent Class method for removing {\em instance} from class attribute \texttt{Instances}
\\

\texttt{\textbf{Compile}()} 
\\
\indent Class method for writing all entries of \texttt{Instances} to \texttt{functionlib.cpp} and compiling function library
\\

\texttt{\textbf{Instances}} 
\\
\indent Class attribute of type \texttt{dict} for storing instances of class 
\\

\noindent {\em class} \texttt{network.\textbf{VertexFunction}(Function}, {\em name}, {\em dimension}, {\em num\_parameters}{)}
\\

\texttt{\textbf{get\_vars}()} 
\\
\indent Returns \texttt{list} of length {\em dimension} $+1$ containing symbolic values for time and variables 
\\

\texttt{\textbf{get\_params}()} 
\\
\indent Returns \texttt{list} of length {\em num\_parameters} containing symbolic values for parameters 
\\

\texttt{\textbf{set\_expression}(}{\em i}, {\em expression}\texttt{)} 
\\
\indent Assigns {\em expression} to $(i-1)$th element of \texttt{expression} attribute of \texttt{self} 
\\

\texttt{\textbf{dimension}} 
\\
\indent Attribute storing {\em dimension} assigned to \texttt{self}
\\

\texttt{\textbf{num\_parameters}} 
\\
\indent Attribute storing {\em num\_parameters} assigned to \texttt{self}
\\

\texttt{\textbf{expression}} 
\\
\indent Attribute storing the symbolic expression assigned to \texttt{self}
\\

\noindent {\em class} \texttt{network.\textbf{EdgeFunction}(Function}, {\em name}, {\em in\_dimension}, {\em out\_dimension}, {\em num\_parameters}{)}
\\

\texttt{\textbf{get\_in\_vars}()} 
\\
\indent Returns \texttt{list} of length {\em in\_dimension} $+1$ containing symbolic values for time and input variables 
\\

\texttt{\textbf{get\_out\_vars}()} 
\\
\indent Returns \texttt{list} of length {\em out\_dimension} $+1$ containing symbolic values for time and output variables 
\\

\texttt{\textbf{get\_params}()} 
\\
\indent Returns \texttt{list} of length {\em num\_parameters} containing symbolic values for parameters 
\\

\texttt{\textbf{set\_expression}(}{\em i}, {\em expression}\texttt{)} 
\\
\indent Assigns {\em expression} to $(i-1)$th element of \texttt{expression} attribute of \texttt{self} 
\\

\texttt{\textbf{dimension}} 
\\
\indent Attribute storing {\em in\_dimension} assigned to \texttt{self}
\\

\texttt{\textbf{out\_dimension}} 
\\
\indent Attribute storing {\em out\_dimension} assigned to \texttt{self}
\\

\texttt{\textbf{num\_parameters}} 
\\
\indent Attribute storing {\em num\_parameters} assigned to \texttt{self}
\\

\texttt{\textbf{expression}} 
\\
\indent Attribute storing the symbolic expression assigned to \texttt{self}
\\

\subsection{\texttt{network}}
The module \texttt{network} provides
\begin{itemize}
\item A \texttt{VertexValue} class encapsulating vertex values
\item An \texttt{EdgeValue} class encapsulating edge values
\item A \texttt{PyVertex} class encapsulating vertex descriptors
\item A \texttt{PyGraph} class encapsulating directed graphs
\end{itemize}
This module is implemented in {\em C++} and exposed to {\em Python} using {\em Boost.Python}.
\\

\noindent {\em class} \texttt{network.\textbf{VertexValue}()}
\\

\texttt{\textbf{set\_function}(}{\em function}\texttt{)} 
\\
\indent Takes \texttt{VertexFunction} object {\em function} as an argument and assigns to \texttt{self}
\\

\texttt{\textbf{set\_init\_conds}(}{\em initial\_conditions}\texttt{)} 
\\
\indent Takes \texttt{list} {\em initial\_conditions} as an argument and assigns to \texttt{self}
\\

\texttt{\textbf{set\_params}(}{\em parameter\_values}\texttt{)} 
\\
\indent Takes \texttt{list} {\em parameter\_values} as an argument and assigns to \texttt{self}
\\

\texttt{\textbf{function\_id}} 
\\
\indent Attribute storing name of the \texttt{VertexFunction} object assigned to \texttt{self}
\\

\texttt{\textbf{user\_data}} 
\\
\indent Attribute to be used for assigning customised data to a vertex
\\

\noindent {\em class} \texttt{network.\textbf{EdgeValue}()}
\\

\texttt{\textbf{set\_function}(}{\em function}\texttt{)} 
\\
\indent Takes \texttt{EdgeFunction} object {\em function} as an argument and assigns to \texttt{self}
\\

\texttt{\textbf{set\_params}(}{\em parameter\_values}\texttt{)} 
\\
\indent Takes \texttt{list} {\em parameter\_values} as an argument and assigns to \texttt{self}
\\

\texttt{\textbf{function\_id}} 
\\
\indent Attribute storing name of the \texttt{EdgeFunction} object assigned to \texttt{self}
\\

\texttt{\textbf{user\_data}} 
\\
\indent Attribute to be used for assigning customised data to an edge
\\

\noindent {\em class} \texttt{network.\textbf{PyVertex}(}{\em name}\texttt{)}
\\

\texttt{\textbf{get\_id}()} 
\\
\indent Returns \texttt{name} attribute of \texttt{self}
\\

\texttt{\textbf{add\_neighbor}(}{\em neighbor}\texttt{)} 
\\
\indent Adds empty \texttt{EdgeValue} object with key {\em neighbor} to \texttt{connected\_to} attribute of \texttt{self}
\\

\texttt{\textbf{remove\_neighbor}(}{\em neighbor}\texttt{)} 
\\
\indent Removes \texttt{EdgeValue} object with key {\em neighbor} from \texttt{connected\_to} attribute of \texttt{self}
\\

\texttt{\textbf{neighbors}()} 
\\
\indent Returns a \texttt{list} of keys in \texttt{connected\_to} attribute of \texttt{self}
\\

\texttt{\textbf{get\_value}()} 
\\
\indent Returns \texttt{VertexValue} attribute \texttt{value} of \texttt{self}
\\

\texttt{\textbf{set\_value}(}{\em value}\texttt{)}  
\\
\indent Takes a \texttt{VertexValue} object {\em value} as an argument and assigns to \texttt{value} attribute of \texttt{self}
\\

\texttt{\textbf{get\_outvalue}(}{\em neighbor}\texttt{)}  
\\
\indent Returns \texttt{EdgeValue} object with key {\em neighbor} from \texttt{connected\_to} attribute of \texttt{self}
\\

\texttt{\textbf{set\_outvalue}(}{\em neighbor}, {\em outvalue}\texttt{)}  
\\
\indent Assign \texttt{EdgeValue} object {\em outvalue} to entry with key {\em neighbor} in  \texttt{connected\_to} attribute of \texttt{self}
\\

\texttt{\textbf{name}} 
\\
\indent Attribute storing {\em name} assigned to \texttt{self}
\\

\texttt{\textbf{connected\_to}} 
\\
\indent Attribute storing \texttt{dict} of neighbors assigned to \texttt{self}
\\

\texttt{\textbf{value}} 
\\
\indent Attribute storing \texttt{VertexValue} object assigned to \texttt{self}
\\

\noindent {\em class} \texttt{network.\textbf{PyGraph}(}{\em name}\texttt{)}
\\

\texttt{\textbf{adjacent}(}{\em vertex1}, {\em vertex2}\texttt{)} 
\\
\indent Returns \texttt{True} if {\em vertex2} is a neighbor of {\em vertex1} and \texttt{False} otherwise
\\

\texttt{\textbf{neighbors}(}{\em vertex}\texttt{)} 
\\
\indent Returns a \texttt{list} containing names of {\em vertex}'s neighbors
\\

\texttt{\textbf{add\_vertex}(}{\em name}\texttt{)} 
\\
\indent Adds empty \texttt{PyVertex} object with key {\em name} to \texttt{vertex\_list} attribute of \texttt{self}
\\

\texttt{\textbf{remove\_vertex}(}{\em name}\texttt{)} 
\\
\indent Removes \texttt{PyVertex} object with key {\em name} from \texttt{vertex\_list} attribute of \texttt{self}
\\

\texttt{\textbf{add\_edge}(}{\em vertex1}, {\em vertex2}\texttt{)} 
\\
\indent Sets {\em vertex2} as neighbour of {\em vertex1}. Adds vertices if not already in \texttt{vertex\_list} attribute of \texttt{self}
\\

\texttt{\textbf{remove\_edge}(}{\em vertex1}, {\em vertex2}\texttt{)} 
\\
\indent Removes {\em vertex2} as neighbour of {\em vertex1}
\\

\texttt{\textbf{get\_vertex\_value}(}{\em vertex}\texttt{)} 
\\
\indent Returns \texttt{VertexValue} object associated with {\em vertex}
\\

\texttt{\textbf{set\_vertex\_value}(}{\em vertex}, {\em value}\texttt{)} 
\\
\indent Assigns \texttt{VertexValue} object {\em value} to {\em vertex}
\\

\texttt{\textbf{get\_edge\_value}(}{\em vertex1}, {\em vertex2}\texttt{)} 
\\
\indent Returns \texttt{EdgeValue} object associated with edge from {\em vertex1} to {\em vertex2} 
\\

\texttt{\textbf{set\_edge\_value}(}{\em vertex1}, {\em vertex2}, {\em value}\texttt{)} 
\\
\indent Assigns \texttt{EdgeValue} object {\em value} to edge from {\em vertex1} to {\em vertex2} 
\\

\texttt{\textbf{simulate}(}{\em tstart}, {\em tstop}, {\em tout}, {\em solver} = \texttt{False)} 
\\
\indent Simulate associated coupled cell system over interval [{\em tstart}, {\em tstop}] with outputs separated by {\em tout}  
\\

\texttt{\textbf{name}} 
\\
\indent Attribute storing {\em name} assigned to \texttt{self}
\\

\texttt{\textbf{vertex\_list}} 
\\
\indent Attribute storing \texttt{dict} of \texttt{PyVertex} objects assigned to \texttt{self}
\\

\texttt{\textbf{num\_vertices}} 
\\
\indent Attribute storing number of \texttt{PyVertex} objects assigned to \texttt{self}
\\

\texttt{\textbf{rel\_tolerance}} 
\\
\indent Attribute storing rel tolerance value (default $10^{-4}$) for simulation of \texttt{self}
\\

\texttt{\textbf{abs\_tolerance}} 
\\
\indent Attribute storing abs tolerance value (default $10^{-4}$) for simulation of \texttt{self}
\\

\begin{appendix}
\section{Acknowledgements}
DST is a Simons Foundation Fellow of the Life Sciences Research Foundation.

\end{appendix}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\bibliographystyle{plain}

\begin{thebibliography}{99}
\bibitem{Hindmarsh05} Hindmarsh AC, Brown PN, Grant KE, Lee SL, Serban R, Shumaker DE, \& Woodward CS (2005) SUNDIALS: Suite of Nonlinear and Differential/Algebraic Equation Solvers. {\em ACM Trans. Math. Softw.} 31: 363-396
\bibitem{Abrahams03} Abrahams D \& Grosse-Kunstleve RW (2003) Building Hybrid Systems with Boost.Python. \url{http://boostorg.github.io/python/doc/html/article.html}
\bibitem{Golubitsky02} Golubitsky M \& Stewart I (2002) Patterns of Oscillation in Coupled Cell Systems. {\em In: Newton P., Holmes P., Weinstein A. (eds) Geometry, Mechanics, and Dynamics. Springer, New York, NY}
\bibitem{Stewart03} Stewart I, Golubitsky M, \& Pivato M (2003) Symmetry Groupoids and Patterns of Synchrony in Coupled Cell Networks. {\em SIAM J. Appl. Dyn. Syst.} 2: 609-646
\bibitem{Golubitsky06} Golubitsky M, Stewart I, \& T\"{o}r\"{o}k A (2006) Patterns of Synchrony in Coupled Cell Networks with Multiple Arrows. {\em SIAM J. Appl. Dyn. Syst.} 4: 78-100
\bibitem{Tourigny17} Tourigny DS (2017) Networks of planar Hamiltonian systems. {\em Comm. Nonlin. Sci. Numer. Simul.} 53: 263- 277
\bibitem{Plotnikov16} Plotnikov SA, Lehnert J, Fradkov AL, \& Sc\"{o}ll E (2016) Synchronization in heterogenous FitzHugh-Nagumo networks with hierarchical architecture. {\em Phys. Rev.} E 94: 012203
\end{thebibliography}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{document}
