# Copyright (C) 2018, 2019 Columbia University Irving Medical Center,
#     New York, USA

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

FROM ubuntu:19.10

ARG NPROC=1
# ARG MPI_VERSION="4.0"
ARG BOOST_VERSION="1.68.0"
ARG SUNDIALS_VERSION="5.0.0-dev.1"

ENV PYTHONUNBUFFERED=1

WORKDIR /opt

RUN set -eux \
    && apt-get update \
    && apt-get install --yes \
        build-essential \
        cmake \
 	    python3-dev \
 	    python3-pip \
	    libgmp-dev \
            curl \
    && python3 -m pip install --upgrade pip setuptools wheel numpy sympy \
    && rm -rf /.cache/pip /tmp/pip* \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

COPY scripts/ ./

RUN set -eux \
#   && bash build_ompi.sh \
    && bash build_boost.sh \
    && bash build_sundials.sh

COPY . ./

RUN set -eux \
    && python3 -m pip install . \
    && rm -rf /.cache/pip /tmp/pip*

# ENV OMPI_MCA_btl_vader_single_copy_mechanism=none # enable for mpirun from root
WORKDIR /opt/examples
