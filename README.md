# CCSS: Coupled Cell System Simulator

*CCSS* is a work-in-progress project toward a freely available software package that provides an efficient and accessible platform for building, manipulating, and simulating coupled cell systems. *CCSS* is designed in an object-oriented fashion, implemented using a combination of *C++* and *Python* in order to meet the performance demands of numerically simulating large dynamical systems while retaining a user-interface based on an intuitive scripting language. Simulations are performed using [SUNDIALS CVODE](https://computation.llnl.gov/projects/sundials/cvode), a robust solver with variable-order, variable-step multistep methods that can effectively integrate both stiff and non-stiff ordinary differential equations (ODEs). To avoid the user having to interact with the lower-level programming language directly, *CCSS* modules have been exposed to *Python* to conveniently build and simulate a wide range of coupled cell systems.

The current implementation is intended only to capture structural aspects of the user interface meaning that numerical integration algorithms have not yet been optimised for speed. Future implementations will take advantage of the dev capabilities of [SUNDIALS](https://computation.llnl.gov/projects/sundials) version 5.0.0-dev.1 and the [Parallel Boost Graph Library](https://www.boost.org/doc/libs/1_71_0/libs/graph_parallel/doc/html/index.html) to leverage the inherent network structure of coupled cell systems for parallelisation. Parallelisation will be based on [Open MPI](https://www.open-mpi.org/) and the option to incorporate MPI test cases into the build process is therefore supplied with this repository.

## Building and installation

*CCSS* runs on *Linux* and *OSX*. A [Dockerfile](./Dockerfile) is provided for building a [Docker](https://docs.docker.com/) image to run the software from an interactive container. The image can be built in one step by issuing the command:
```
make build
```
from the root of this repository. It can then be started using
```
make run
```

Alternatively, *CCSS* can be built and installed directly from source once the dependencies (below) have been installed correctly. The package can be installed directly from source from the root of this repository by running the command:
```
pip install .
```

## Dependencies

Building directly from source depends on having the following packages pre-installed on the user's platform:
* [CMake](https://cmake.org/) version 2.8.3 or higher
* [Boost](https://www.boost.org/) version 1.68.0
* [Python](https://www.python.org/) version 3.6 or higher with [SymPy](https://www.sympy.org/en/index.html) installed
* [SUNDIALS](https://computation.llnl.gov/projects/sundials) version 5.0.0-dev.1
* a suitable *C++* compiler, e.g. [GCC](http://gcc.gnu.org/)

*Boost* and *SUNDIALS* are most conveniently obtained, built and installed running the scripts [build_boost](./scripts/build_boost.sh) and [build_sundials](./scripts/build_sundials.sh), respectively, from the root of this repository. Version number variables should be exported prior to running these scripts.

## Documentation and examples

The file [ccss_guide](./docs/guide/ccss_guide.pdf) contains extensive documentation for running the current version and [ccss_examples](./docs/examples/ccss_examples.pdf) provides detailed explanations of the example scripts found in the directory [examples](./examples).
